# Release Notes

This project adheres to [Semantic Versioning](https://semver.org/).

## 13.17.0

- Updated `cjk-related.json` data file (10.33).
- Updated `cjk-ids.json` data file (3.70).
- Updated `BabelStoneHanPUA.woff2` font file (v1.463).
- Updated `Electron` to version `33.4.3`.

## 13.16.0

- Updated `cjk-related.json` data file (10.27).
- Updated `cjk-ids.json` data file (3.65).
- Updated `Electron` to version `33.4.2`.

## 13.15.0

- Updated `cjk-related.json` data file (10.19).
- Updated `cjk-ids.json` data file (3.58).

## 13.14.0

- Updated `cjk-related.json` data file (10.9).
- Updated `cjk-ids.json` data file (3.51).
- Updated `Electron` to version `33.4.1`.

## 13.13.0

- Updated `cjk-related.json` data file (10.0).
- Updated `cjk-ids.json` data file (3.42).

## 13.12.0

- Updated `cjk-related.json` data file (9.93).
- Updated `cjk-ids.json` data file (3.36).
- Updated `Electron` to version `33.4.0`.

## 13.11.0

- Added dynamic links with copy-to-clipboard contextual menus to Unihan characters of the **Look Up IDS** feature of **CJK Components** utility.
- Added `Copy Code Points` item to contextual menus of **Parse IDS** feature of **CJK Components** utility.
- Added `Copy Code Points` item to contextual menus of **CJK Variations** and **Pan-CJK Font Variants** utilities.
- Updated `cjk-related.json` data file (9.89).
- Updated `cjk-ids.json` data file (3.32).
- Updated `BabelStoneHanPUA.woff2` font file (v1.462).

## 13.10.0

- Updated `cjk-related.json` data file (9.85).
- Updated `cjk-ids.json` data file (3.29).
- Updated `BabelStoneHanPUA.woff2` font file (v1.461).
- Updated `Electron` to version `33.3.2`.

## 13.9.0

- Updated `cjk-related.json` data file (9.78).
- Updated `cjk-ids.json` data file (3.25).

## 13.8.0

- Added **Pan-CJK Font Variants** utility.
- Updated `cjk-related.json` data file (9.71).
- Updated `cjk-ids.json` data file (3.18).

## 13.7.0

- Updated `cjk-related.json` data file (9.60).
- Updated `cjk-ids.json` data file (3.10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.460).
- Updated `Electron` to version `33.3.1`.
- Updated copyright years.

## 13.6.0

- Updated instructions of the **CJK Components** utility.
- Updated `cjk-related.json` data file (9.52).
- Updated `cjk-ids.json` data file (3.3).
- Updated `BabelStoneHanPUA.woff2` font file (v1.459).

## 13.5.0

- Updated `cjk-related.json` data file (9.47).
- Updated `cjk-ids.json` data file (3.1): replaced `UCS2003` with `Q`.

## 13.4.0

- Updated reference links of the **CJK Components** utility.
- Updated `cjk-ids.json` data file (2.18).
- Updated `cjk-related.json` data file (9.44).

## 13.3.0

- Updated `cjk-ids.json` data file (2.11).
- Updated `cjk-related.json` data file (9.37).

## 13.2.0

- Updated `cjk-related.json` data file (9.32).
- Updated `cjk-ids.json` data file (2.6).
- Updated `BabelStoneHanPUA.woff2` font file (v1.458).

## 13.1.1

- Fixed handling of font family and font color of text in graphs in the **Look Up IDS** and **Parse IDS** features of the **CJK Components** utility.
- Fixed handling of the new IDS compact format in the **CJK Components** utility.
- Fixed display of supported Unicode version in the navigation sidebar.

## 13.1.0

- Formalized easter egg feature.
- Updated `cjk-related.json` data file (9.26).
- Updated `cjk-ids.json` data file (2.2).
- Updated `BabelStoneHanPUA.woff2` font file (v1.457-2).
- Updated `Electron` to version `33.2.1`.

## 13.0.0

- Updated `cjk-ids.json` data file (2.0): revamped compact format.
- Updated code for new IDS format accordingly.
- Updated `cjk-related.json` data file (9.24).

## 12.1.0

- Updated `cjk-ids.json` data file (1.2).
- Updated `cjk-related.json` data file (9.22).
- Updated `regexpu-core` module to version `6.2.0`.

## 12.0.0

- Added Other data field to the **CJK Radicals** utility.
- Added MIT-licensed `cjk-ids.json` data file (1.0) as a replacement for `IDS.TXT` (custom 2024-11-02); updated code accordingly.
- Updated `cjk-related.json` data file (9.20).

## 11.4.0

- Revamped IDS data parsing format.
- Updated `cjk-related.json` data file (9.18).
- Updated `IDS.TXT` data file (custom 2024-11-02).

## 11.3.0

- Updated `cjk-related.json` data file (9.14).
- Updated `IDS.TXT` data file (custom 2024-10-31).
- Updated `BabelStoneHanPUA.woff2` font file (v1.456).
- Updated `Electron` to version `33.2.0`.

## 11.2.0

- Added Normalized data field to the **CJK Radicals** utility.
- Updated `cjk-related.json` data file (9.9).

## 11.1.0

- Added individual code points to all glyphs of the **CJK Radicals** and **CJK Strokes** utilities.
- Added Equivalent Unified Ideograph data field to the **CJK Strokes** utility.
- Updated `cjk-related.json` data file (9.8).
- Updated `IDS.TXT` data file (custom 2024-10-25).
- Updated `BabelStoneHanPUA.woff2` font file (v1.454).

## 11.0.0

- Added new **CJK Strokes** utility.
- Improved live search in the **CJK Radicals** utility.

## 10.3.0

- Allowed CJK stroke lookup in the **CJK Local Fonts** utility.
- Updated `cjk-related.json` data file (9.5).
- Updated `IDS.TXT` data file (custom 2024-10-22).

## 10.2.0

- Updated `cjk-related.json` data file (9.3).
- Updated `IDS.TXT` data file (custom 2024-10-21).
- Updated `BabelStoneHanPUA.woff2` font file (v1.453).
- Updated `Electron` to version `32.2.2`.

## 10.1.0

- Added Whole Word user option to the  **CJK Radicals** utility.
- Updated `cjk-related.json` data file (8.21).
- Updated `IDS.TXT` data file (custom 2024-10-17).

## 10.0.0

- Added new **CJK Radicals** utility.
- Updated `cjk-related.json` data file (8.20).
- Updated `IDS.TXT` data file (custom 2024-10-16).
- Updated `BabelStoneHanPUA.woff2` font file (v1.452).

## 9.5.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v16.0.2).
- Improved display of Chinese radicals in the **CJK Components** and **CJK Local Fonts** utilities.
- Updated `cjk-related.json` data file (8.17).
- Updated `IDS.TXT` data file (custom 2024-10-12).
- Updated `BabelStoneHanPUA.woff2` font file (v1.451).
- Updated `Electron` to version `32.2.0`.
- Updated `Electron Builder` to version `25.1.8`.

## 9.4.0

- Updated `cjk-related.json` data file (8.14).
- Updated `IDS.TXT` data file (custom 2024-09-30).
- Updated `Electron Builder` to version `25.1.7`.

## 9.3.0

- Updated `cjk-related.json` data file (8.8).
- Updated `IDS.TXT` data file (custom 2024-09-24).
- Updated `BabelStoneHanPUA.woff2` font file (v1.450).

## 9.2.0

- Checked and updated all reference links.
- Updated `cjk-related.json` data file (8.3).
- Updated `IDS.TXT` data file (custom 2024-09-17).

## 9.1.0

- Updated `cjk-related.json` data file (8.0): updated sets of `Simplified-Traditional` characters to **Unicode 16.0**.
- Updated `IDS.TXT` data file (custom 2024-09-16).
- Updated `regexpu-core` module to version `6.1.1`.
- Updated `Electron` to version `32.1.2`.

## 9.0.0

- Updated Unicode data files to version `16.0.0`.
- Updated `regexpu-core` module to version `6.1.0`.
- Updated samples in the **CJK Sources** utility.
- Updated CJK sample script of the **JavaScript Runner** utility: `Unzip CJK Sources Data Files to User Data Directory`.
- Updated `cjk-related.json` data file (7.2).
- Updated `IDS.TXT` data file (custom 2024-09-15).
- Updated `BabelStoneHanPUA.woff2` font file (v1.448-2).
- Updated `Electron` to version `32.1.1`.

## 8.16.0

- Updated `IDS.TXT` data file (custom 2024-09-07).
- Updated `cjk-related.json` data file (6.22).
- Updated `BabelStoneHanPUA.woff2` font file (v1.446).
- Updated `Electron` to version `32.0.2`.
- Updated `Electron Builder` to version `25.0.5`.

## 8.15.0

- Updated `cjk-related.json` data file (6.17).
- Updated `IDS.TXT` data file (custom 2024-08-31).
- Updated `BabelStoneHanPUA.woff2` font file (v1.444-2).

## 8.14.0

- Updated `cjk-related.json` data file (6.8).
- Updated `IDS.TXT` data file (custom 2024-08-22).
- Updated `BabelStoneHanPUA.woff2` font file (v1.442).
- Updated `Electron` to version `32.0.1`.

## 8.13.0

- Updated `cjk-related.json` data file (6.0).
- Updated `IDS.TXT` data file (custom 2024-08-16).
- Updated `BabelStoneHanPUA.woff2` font file (v1.441).
- Updated `Electron` to version `31.4.0`.

## 8.12.0

- Updated `cjk-related.json` data file (5.57).
- Updated `IDS.TXT` data file (custom 2024-08-08).
- Updated `BabelStoneHanPUA.woff2` font file (v1.439).

## 8.11.1

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on a slightly updated `BabelStone Han` font (v16.0.1).

## 8.11.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v16.0.1).
- Updated `cjk-related.json` data file (5.48).
- Updated `IDS.TXT` data file (custom 2024-07-30).
- Updated `BabelStoneHanPUA.woff2` font file (v1.438).
- Updated `Electron` to version `31.3.1`.

## 8.10.0

- Refactored code: moved duplicate code to Unicode and Unihan lib modules.
- Updated `cjk-related.json` data file (5.42).
- Updated `IDS.TXT` data file (custom 2024-07-24).
- Updated `BabelStoneHanPUA.woff2` font file (v1.435).
- Updated `Electron` to version `31.3.0`.

## 8.9.0

- Updated `cjk-related.json` data file (5.35).
- Updated `IDS.TXT` data file (custom 2024-07-14).
- Updated `BabelStoneHanPUA.woff2` font file (v1.433).
- Updated `Electron` to version `31.2.1`.

## 8.8.0

- Updated `cjk-related.json` data file (5.27).
- Updated `IDS.TXT` data file (custom 2024-07-10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.432-2).
- Updated `Electron` to version `31.2.0`.

## 8.7.0

- Updated `cjk-related.json` data file (5.24).
- Updated `IDS.TXT` data file (custom 2024-07-01).
- Updated `BabelStoneHanPUA.woff2` font file (v1.430).
- Updated `Electron` to version `31.1.0`.

## 8.6.0

- Updated `cjk-related.json` data file (5.19).
- Updated `IDS.TXT` data file (custom 2024-06-25).
- Updated `BabelStoneHanPUA.woff2` font file (v1.428).
- Updated `IDS_PUA.TXT` data file (v1.428).
- Updated `Electron` to version `31.0.2`.

## 8.5.0

- Updated `cjk-related.json` data file (5.13).
- Updated `IDS.TXT` data file (custom 2024-06-08).
- Updated `Electron` to version `31.0.1`.

## 8.4.0

- Used semi-colon as multiple-field separator for notes in the **Look Up IDS** feature of the **CJK Components** utility.
- Updated `cjk-related.json` data file (5.8).
- Updated `IDS.TXT` data file (custom 2024-06-05).
- Updated `Electron` to version `30.1.0`.

## 8.3.0

- Updated `cjk-related.json` data file (5.7).
- Updated `IDS.TXT` data file (custom 2024-06-01).
- Updated `BabelStoneHanPUA.woff2` font file (v1.427).
- Updated `Electron` to version `30.0.9`.

## 8.2.0

- Updated `cjk-related.json` data file (5.0): added all-purpose `Related` category.
- Updated `IDS.TXT` data file (custom 2024-05-25).
- Updated `Electron` to version `30.0.8`.

## 8.1.0

- Updated samples of KP-source suboptimal mappings in the **CJK Sources** utility.
- Updated `Glyph Defects` samples in the **CJK Sources** utility.
- Updated `cjk-related.json` data file (4.21).
- Updated `IDS.TXT` data file (custom 2024-05-18).
- Updated `Electron` to version `30.0.6`.

## 8.0.0

- Updated `cjk-related.json` data file (4.7).
- Updated `IDS.TXT` data file (custom 2024-05-10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.426).
- Updated `Electron` to version `30.0.3`.

## 7.21.0

- Updated `cjk-related.json` data file (3.120).
- Updated `IDS.TXT` data file (custom 2024-05-03).
- Updated `BabelStoneHanPUA.woff2` font file (v1.425).
- Updated `Electron` to version `30.0.2`.

## 7.20.0

- Updated `Glyph Defects` samples in the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.114).
- Updated `IDS.TXT` data file (custom 2024-04-26).

## 7.19.0

- Updated `Glyph Defects` samples in the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.107).
- Updated `IDS.TXT` data file (custom 2024-04-19).
- Updated `Electron` to version `30.0.1`.

## 7.18.0

- Updated samples of KP-source suboptimal mappings in the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.102).
- Updated `IDS.TXT` data file (custom 2024-04-13).
- Updated `BabelStoneHanPUA.woff2` font file (v1.423).
- Updated `Electron` to version `29.3.0`.

## 7.17.0

- Updated `cjk-related.json` data file (3.98).
- Updated `IDS.TXT` data file (custom 2024-04-09).
- Updated `BabelStoneHanPUA.woff2` font file (v1.422-2).

## 7.16.0

- Updated samples of KP-source suboptimal mappings in the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.92).
- Updated `IDS.TXT` data file (custom 2024-04-03).
- Updated `IDS_PUA.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file (v1.422).
- Updated `Electron` to version `29.2.0`.

## 7.15.0

- Added samples of Extension I duplicate glyphs to the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.87).
- Updated `IDS.TXT` data file (custom 2024-03-29).

## 7.14.0

- Added samples of T-source oddball radical 162 to the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.86).
- Updated `IDS.TXT` data file (custom 2024-03-27).
- Updated `BabelStoneHanPUA.woff2` font file (v1.419).

## 7.13.0

- Updated samples of KP-source suboptimal mappings in the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.85).
- Updated `IDS.TXT` data file (custom 2024-03-25).
- Updated `BabelStoneHanPUA.woff2` font file (v1.418).
- Updated `Electron` to version `29.1.6`.

## 7.12.0

- Updated `cjk-related.json` data file (3.82).
- Updated `IDS.TXT` data file (custom 2024-03-20).
- Updated `BabelStoneHanPUA.woff2` font file (v1.417).
- Updated `Electron` to version `29.1.5`.

## 7.11.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v15.1.4).
- Updated `cjk-related.json` data file (3.74).
- Updated `IDS.TXT` data file (custom 2024-03-15).
- Updated `BabelStoneHanPUA.woff2` font file (v1.413).
- Updated `Electron` to version `29.1.4`.

## 7.10.0

- Updated samples of the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.69).
- Updated `IDS.TXT` data file (custom 2024-03-11).
- Updated `BabelStoneHanPUA.woff2` font file (v1.412).
- Updated `Electron` to version `29.1.3`.

## 7.9.0

- Updated `cjk-related.json` data file (3.63).
- Updated `IDS.TXT` data file (custom 2024-03-05).

## 7.8.0

- Updated `cjk-related.json` data file (3.58).
- Updated `IDS.TXT` data file (custom 2024-02-29).
- Updated `BabelStoneHanPUA.woff2` font file (v1.411).
- Updated `Electron` to version `29.1.0`.
- Updated `Electron Builder` to version `24.13.3`.

## 7.7.0

- Updated `cjk-related.json` data file (3.52).
- Updated `IDS.TXT` data file (custom 2024-02-25).
- Updated `BabelStoneHanPUA.woff2` font file (v1.410).
- Updated `Electron` to version `29.0.1`.

## 7.6.0

- Updated `cjk-related.json` data file (3.43).
- Updated `IDS.TXT` data file (custom 2024-02-18).
- Updated `BabelStoneHanPUA.woff2` font file (v1.407).
- Updated `Electron` to version `29.0.0`.
- Updated `Electron Builder` to version `24.12.0`.

## 7.5.0

- Added missing glyph for `UTC-00355` (Unicode 15.1 errata notice) and updated samples of the **CJK Sources** utility.
- Updated `cjk-related.json` data file (3.36).
- Updated `IDS.TXT` data file (custom 2024-02-11), adding partial support for the ≈24,000 new KP-source glyphs in **Unicode 15.1**.
- Updated `BabelStoneHanPUA.woff2` font file (v1.406).
- Updated `Electron` to version `28.2.2`.

## 7.4.0

- Updated `cjk-related.json` data file (3.16).
- Updated `IDS.TXT` data file (custom 2024-01-28): used new source-ordering, consistent with the **CJK Sources** utility.
- Updated `BabelStoneHanPUA.woff2` font file (v1.402).

## 7.3.0

- Added `VS` (Variation Selector) drop-down menu to the **CJK Local Fonts** utility.
- Updated `cjk-related.json` data file (3.11).
- Updated `IDS.TXT` data file (custom 2024-01-24).
- Updated `@electron/remote` module to version `2.1.2`.
- Updated `Electron` to version `28.2.0`.

## 7.2.0

- Updated `cjk-related.json` data file (3.7).
- Updated `IDS.TXT` data file (custom 2024-01-17).
- Updated `BabelStoneHanPUA.woff2` font file (v1.401).
- Updated `Electron` to version `28.1.4`.

## 7.1.0

- Updated `cjk-related.json` data file (v3.4): updated sets of `Simplified-Traditional` characters to **Unicode 15.1**.
- Updated `IDS.TXT` data file (custom 2024-01-13).
- Updated `Electron` to version `28.1.3`.
- Updated `BabelStoneHanPUA.woff2` font file (v1.400).

## 7.0.0

- Added partial support for **Unicode 15.1**, awaiting full support in a forthcoming version of the `Electron` framework.
- Updated `cjk-related.json` data file (v3.0): replaced `↔↷⊖` operators with Unicode 15.1 characters `⿾⿿㇯`.
- Updated `IDS.TXT` data file (custom 2024-01-09): added ideographic description sequences for `CJKUI Extension I`, and replaced `↔↷⊖` operators with Unicode 15.1 characters `⿾⿿㇯`

## 6.16.0

- Updated the R/S collation key used to sort results in the **Match IDS** feature of the **CJK Components** utility.
- Updated `cjk-related.json` data file (v2.161).
- Updated `IDS.TXT` data file (custom 2024-01-06).
- Updated `BabelStoneHanPUA.woff2` font file (v1.398).
- Updated `Electron` to version `28.1.2`.

## 6.15.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v15.1.3).
- Added `BSH_IVS.TXT` data file.
- Updated `cjk-related.json` data file (v2.154).
- Updated `IDS.TXT` data file (custom 2024-01-02).
- Updated `IDS_PUA.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file (v1.395).
- Updated copyright years.

## 6.14.0

- Updated `cjk-related.json` data file (v2.147).
- Updated `IDS.TXT` data file (custom 2023-12-27).
- Updated `BabelStoneHanPUA.woff2` font file (v1.394).

## 6.13.0

- Added IRG Source References table to the **CJK Sources** utility.
- Updated `cjk-related.json` data file (v2.137).
- Updated `IDS.TXT` data file (custom 2023-12-21).
- Updated `BabelStoneHanPUA.woff2` font file (v1.392).
- Updated `Electron` to version `28.1.0`.

## 6.12.0

- Updated `cjk-related.json` data file (v2.131).
- Updated `IDS.TXT` data file (custom 2023-12-17).
- Updated `BabelStoneHanPUA.woff2` font file (v1.390).

## 6.11.0

- Updated `cjk-related.json` data file (v2.124).
- Updated `IDS.TXT` data file (custom 2023-12-10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.388).
- Updated `@electron/remote` module to version `2.1.1`.
- Updated `Electron` to version `28.0.0`.

## 6.10.0

- Sorted characters by total stroke count in the linear view of the **CJK Related** utility.
- Updated `cjk-related.json` data file (v2.105).
- Updated `IDS.TXT` data file (custom 2023-12-02).
- Updated `BabelStoneHanPUA.woff2` font file (v1.387).
- Updated `Electron` to version `27.1.3`.

## 6.9.0

- Updated `cjk-related.json` data file (v2.99).
- Updated `IDS.TXT` data file (custom 2023-11-28).
- Updated `BabelStoneHanPUA.woff2` font file (v1.386).

## 6.8.0

- Updated `cjk-related.json` data file (v2.90).
- Updated `IDS.TXT` data file (custom 2023-11-24).
- Updated `BabelStoneHanPUA.woff2` font file (v1.384).
- Updated `Electron Builder` to version `24.9.1`.
- Updated `Electron` to version `27.1.2`.

## 6.7.0

- Updated `cjk-related.json` data file (v2.78).
- Updated `IDS.TXT` data file (custom 2023-11-17).
- Updated `BabelStoneHanPUA.woff2` font file (v1.381).
- Updated `Electron` to version `27.1.0`.

## 6.6.0

- Updated `cjk-related.json` data file (v2.66).
- Updated `IDS.TXT` data file (custom 2023-11-11).
- Updated `BabelStoneHanPUA.woff2` font file (v1.378).

## 6.5.0

- Updated `cjk-related.json` data file (v2.57).
- Updated `IDS.TXT` data file (custom 2023-11-07).
- Updated `BabelStoneHanPUA.woff2` font file (v1.376).
- Updated `@electron/remote` module to version `2.1.0`.
- Updated `Electron` to version `27.0.4`.

## 6.4.0

- Updated `cjk-related.json` data file (v2.47).
- Updated `IDS.TXT` data file (custom 2023-11-02).
- Updated `Electron` to version `27.0.3`.

## 6.3.0

- Added a `Sorted by` selectable user option to the **Match IDS** feature of the **CJK Components** utility, among `Code Points`, `Total Strokes`, and `Radical-Strokes`.
- Updated `cjk-related.json` data file (v2.36).
- Updated `IDS.TXT` data file (custom 2023-10-28).
- Updated `BabelStoneHanPUA.woff2` font file (v1.373).

## 6.2.0

- Updated `cjk-related.json` data file (v2.26).
- Updated `IDS.TXT` data file (custom 2023-10-23).
- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v15.1.2).
- Updated `Electron` to version `27.0.2`.

## 6.1.0

- Updated `cjk-related.json` data file (v2.12).
- Updated `IDS.TXT` data file (custom 2023-10-17).
- Updated `BabelStoneHanPUA.woff2` font file (v1.370).
- Updated `Electron` to version `27.0.1`.

## 6.0.0

- Added `Extended` checkbox to the **CJK Related** utility.
- Updated instructions accordingly.
- Updated `cjk-related.json` data file (v2.1).
- Updated `IDS.TXT` data file (custom 2023-10-12).

## 5.49.0

- Updated `cjk-related.json` data file (v2.0): added explicit *extended* marker (`*`) to unregistered simplified/traditional sets.
- Updated `IDS.TXT` data file (custom 2023-10-11).
- Updated `BabelStoneHanPUA.woff2` font file (v1.368).

## 5.48.0

- Updated `cjk-related.json` data file (v1.221).
- Updated `IDS.TXT` data file (custom 2023-10-09).
- Updated `Electron` to version `27.0.0`.
- Updated `@electron/remote` module to version `2.0.12`.

## 5.47.0

- Updated CJK sample scripts of the **JavaScript Runner** utility: `Unzip CJK Sources Data Files to User Data Directory` and `Unzip CJK Variations Data Files to User Data Directory`.
- Updated `cjk-related.json` data file (v1.216).
- Updated `IDS.TXT` data file (custom 2023-10-07).
- Updated `BabelStoneHanPUA.woff2` font file (v1.364).
- Updated `Electron` to version `26.3.0`.

## 5.46.0

- Moved project code from [GitHub](https://github.com) to [Codeberg](https://codeberg.org).
- Updated `cjk-related.json` data file (v1.214).
- Updated `IDS.TXT` data file (custom 2023-10-01).
- Updated `BabelStoneHanPUA.woff2` font file (v1.361).
- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v15.1.1).
- Updated `Electron Builder` to version `24.6.4`.
- Updated `Electron` to version `26.2.4`.

## 5.45.1

- Used last supported version **Unicode 15.0** multi-block code charts PDF link for sources.
- Updated instructions accordingly in the **CJK Components** and **CJK Sources** utilities.

## 5.45.0

- Updated `cjk-related.json` data file (v1.199).
- Updated `IDS.TXT` data file (custom 2023-08-26).
- Updated `Electron` to version `26.1.0`.

## 5.44.0

- Updated `cjk-related.json` data file (v1.181).
- Updated `IDS.TXT` data file (custom 2023-08-21).
- Updated `BabelStoneHanPUA.woff2` font file (v1.355).
- Updated `Electron Packager` to version `17.1.2`.

## 5.43.0

- Updated `cjk-related.json` data file (v1.166).
- Updated `IDS.TXT` data file (custom 2023-08-15).
- Updated `BabelStoneHanPUA.woff2` font file (v1.354).
- Updated `Electron` to version `26.0.0`.

## 5.42.0

- Updated `cjk-related.json` data file (v1.158).
- Updated `IDS.TXT` data file (custom 2023-08-10).
- Updated `Electron` to version `25.5.0`.

## 5.41.0

- Updated `cjk-related.json` data file (v1.149).
- Updated `IDS.TXT` data file (custom 2023-08-06).

## 5.40.0

- Updated `cjk-related.json` data file (v1.138).
- Updated `IDS.TXT` data file (custom 2023-08-02).
- Updated `Electron` to version `25.4.0`.

## 5.39.0

- Updated `cjk-related.json` data file (v1.125).
- Updated `IDS.TXT` data file (custom 2023-07-28).
- Updated `Electron` to version `25.3.2`.

## 5.38.0

- Updated `cjk-related.json` data file (v1.119).
- Updated `IDS.TXT` data file (custom 2023-07-25).
- Updated `BabelStoneHanPUA.woff2` font file (v1.353).
- Updated `Electron Builder` to version `24.6.3`: fixes vulnerabilities reported by `npm install`.

## 5.37.0

- Enabled wrapping of CJK characters and code points in the linear list view of the **CJK Related** utility.
- Updated `cjk-related.json` data file (v1.110).
- Updated `IDS.TXT` data file (custom 2023-07-21).
- Updated `Electron` to version `25.3.1`.

## 5.36.0

- Updated `cjk-related.json` data file (v1.106).
- Updated `IDS.TXT` data file (custom 2023-07-19).

## 5.35.0

- Added new `Substitution` category to the **CJK Related** utility.
- Updated `cjk-related.json` data file (v1.95).
- Updated `IDS.TXT` data file (custom 2023-07-14).

## 5.34.0

- Updated `cjk-related.json` data file (v1.93).
- Updated `IDS.TXT` data file (custom 2023-07-13).
- Updated `Electron` to version `25.3.0`.

## 5.33.0

- Updated `cjk-related.json` data file (v1.72).
- Updated `IDS.TXT` data file (custom 2023-07-06).
- Updated `BabelStoneHanPUA.woff2` font file (v1.351).

## 5.32.0

- Refactored code and expanded `Insert` contextual menus in the **CJK Components** utility.
- Updated `cjk-related.json` data file (v1.57).
- Updated `IDS.TXT` data file (custom 2023-07-01).

## 5.31.0

- Updated `cjk-related.json` data file (v1.50).
- Updated `IDS.TXT` data file (custom 2023-06-24).

## 5.30.0

- Updated `cjk-related.json` data file (v1.41).
- Updated `IDS.TXT` data file (custom 2023-06-20).
- Updated `BabelStoneHanPUA.woff2` font file (v1.350).
- Updated `Electron` to version `25.2.0`.

## 5.29.0

- Updated `cjk-related.json` data file (v1.26).
- Updated `IDS.TXT` data file (custom 2023-06-16).
- Updated `BabelStoneHanPUA.woff2` font file (v1.349).

## 5.28.0

- Updated `cjk-related.json` data file (v1.17).
- Updated `IDS.TXT` data file (custom 2023-06-13).
- Updated `Electron` to version `25.1.1`.
- Updated `Electron Builder` to version `24.4.0`: fixes `npm install` reported vulnerabilities.

## 5.27.0

- Updated `cjk-related.json` data file (v1.13).
- Updated `IDS.TXT` data file (custom 2023-06-08).
- Updated `BabelStoneHanPUA.woff2` font file (v1.348).

## 5.26.0

- Added marker dynamically to "unregistered" simplified-traditional sets in the **CJK Related** utility.
- Updated `cjk-related.json` data file (v1.5).
- Updated `font-list` module to version `1.5.0`.
- Updated `IDS.TXT` data file (custom 2023-06-06).
- Updated `BabelStoneHanPUA.woff2` font file (v1.346-1).
- Updated `@electron/remote` module to version `2.0.10`.
- Updated `Electron` to version `25.1.0`.

## 5.25.0

- Added subgroups to `Addition` category of the **CJK Related** utility.
- Updated `cjk-related.json` data file (v1.0).
- Updated `IDS.TXT` data file (custom 2023-06-05).

## 5.24.0

- Added explicit `header` and `data` fields to `cjk-related.json`.
- Updated `cjk-related.json` data file (2023-06-02).
- Updated `IDS.TXT` data file (custom 2023-06-02).
- Updated `BabelStoneHanPUA.woff2` font file (v1.346).
- Updated `Electron` to version `25.0.1`.

## 5.23.0

- Added new `Addition` category to the **CJK Related** utility.
- Revamped `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-30).
- Updated `Electron` to version `25.0.0`.

## 5.22.0

- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-28).
- Updated `BabelStoneHanPUA.woff2` font file (v1.345).
- Updated `Electron` to version `24.4.0`.

## 5.21.0

- Refactored display of group strings in the **CJK Related** utility.
- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-22).

## 5.20.0

- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-21).

## 5.19.0

- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-18).
- Updated `BabelStoneHanPUA.woff2` font file (v1.344).
- Updated `Electron` to version `24.3.1`.

## 5.18.0

- Added `Copy Set` contextual menus to the **CJK Related** utility.
- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-15).

## 5.17.0

- Added visual marker (*) for extended shinjitai/kyūjitai sets to the **CJK Related** utility.
- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-12).

## 5.16.0

- Updated `cjk-related.json` data file; used '*' marker for extended shinjitai/kyūjitai sets.
- Updated `IDS.TXT` data file (custom 2023-05-11).
- Updated `Electron` to version `24.3.0`.

## 5.15.0

- Updated `cjk-related.json` data file (including layout fixes).
- Updated `IDS.TXT` data file (custom 2023-05-10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.343).

## 5.14.0

- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-09).
- Updated reference links of the **CJK Related** utility.

## 5.13.0

- Combined two CJK related sample scripts of the **JavaScript Runner** utility into one: `Export CJK Related Data to JSON and Text Files`.
- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-05-05).
- Updated `Electron` to version `24.2.0`.

## 5.12.0

- Updated CJK sample scripts: `Export CJK Related Data to JSON File` and `Export CJK Related Data to Text Files` of the **JavaScript Runner** utility: added explicit MIT license.
- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-04-29).

## 5.11.0

- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-04-27).
- Updated `BabelStoneHanPUA.woff2` font file (v1.342).
- Updated `Electron` to version `24.1.3`.

## 5.10.0

- Added two CJK sample scripts to the **JavaScript Runner** utility: `Export CJK Related Data to JSON File` and `Export CJK Related Data to Text Files`.
- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-04-23).
- Updated `BabelStoneHanPUA.woff2` font file (v1.341).

## 5.9.0

- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-04-21).

## 5.8.0

- Updated `cjk-related.json` data file.
- Updated `IDS.TXT` data file (custom 2023-04-18).
- Updated `Electron` to version `24.1.2`.

## 5.7.0

- Expanded lists of related CJK characters; updated comments format.
- Updated `IDS.TXT` data file (custom 2023-04-14).

## 5.6.0

- Expanded lists of related CJK characters.
- Updated `IDS.TXT` data file (custom 2023-04-13).
- Updated `Electron` to version `24.1.1`.

## 5.5.0

- Expanded lists of related CJK characters.
- Updated `IDS.TXT` data file (custom 2023-04-10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.340).

## 5.4.0

- Improved and expanded lists of related CJK characters.
- Updated `IDS.TXT` data file (custom 2023-04-07).
- Updated `Electron` to version `24.0.0`.

## 5.3.0

- Expanded lists of related CJK characters.
- Updated `IDS.TXT` data file (custom 2023-04-03).

## 5.2.0

- Expanded lists of related CJK characters; added new group: `⿲①②①・⿲②①②` to `Layout` category.
- Updated `IDS.TXT` data file (custom 2023-03-30).
- Updated `Electron` to version `23.2.1`.

## 5.1.0

- Added code points to the *detailed* list view of the **CJK Related** utility, and improved overall "navigability".
- Expanded lists of related CJK characters (still WIP).
- Added two categories to the `cjk-related.json` data file: `Shinjitai/Kyūjitai`, `Simplified/Traditional`.
- Updated `IDS.TXT` data file (custom 2023-03-24).
- Updated `BabelStoneHanPUA.woff2` font file (v1.338).
- Updated `Electron` to version `23.2.0`.

## 5.0.0

- Added a *detailed* list view to the **CJK Related** utility, made of explicit categories.
- Reorganized the `cjk-related.json` data file into 9 categories: `Confusable`, `Duplicate`, `Layout`, `Not-Unifiable`, `Parts`, `Radical`, `Repeated`, `Series`, `Unifiable`.
- Updated `IDS.TXT` data file (custom 2023-03-19).
- Updated `Electron` to version `23.1.4`.
- Updated `Electron Builder` to version `23.6.0`.
- Updated `Electron Packager` to version `17.1.1`.

## 4.15.0

- Revamped `cjk-related.json` experimental data file: sets of CJK characters are now ordered logically for series, and by component stroke count otherwise.
- Updated `IDS.TXT` data file (custom 2023-03-16).

## 4.14.0

- Updated `cjk-related.json` experimental data file: refined `layout` category; added new `series` category.
- Updated `IDS.TXT` data file (custom 2023-03-10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.336).
- Updated `regexpu-core` module to version `5.3.2`.

## 4.13.0

- Updated `cjk-related.json` experimental data file.
- Updated `IDS.TXT` data file (custom 2023-03-09).

## 4.12.0

- Revamped structure of `cjk-related.json` experimental data file.
- Updated `IDS.TXT` data file (custom 2023-03-06).

## 4.11.0

- Expanded list of related CJK characters.
- Updated `IDS.TXT` data file (custom 2023-03-04).

## 4.10.0

- Expanded list of related CJK characters.
- Updated `IDS.TXT` data file (custom 2023-03-01).
- Updated `BabelStoneHanPUA.woff2` font file (v1.335).

## 4.9.0

- Renamed the **CJK Confusables** utility to **CJK Related**.
- Renamed the `other` category to `confusable` in the list of sets of related CJK characters.

## 4.8.0

- Updated list of sets of confusable CJK characters: completed the `repeated` category.
- Updated `IDS.TXT` data file (custom 2023-02-26).

## 4.7.0

- Updated list of sets of confusable CJK characters.
- Updated `IDS.TXT` data file (custom 2023-02-24).
- Updated `BabelStoneHanPUA.woff2` font file (v1.334).

## 4.6.0

- Added `repeated` category to the list of sets of confusable CJK characters (疊字・理義字).
- Updated `IDS.TXT` data file (custom 2023-02-21).

## 4.5.0

- Updated list of sets of confusable CJK characters.
- Updated `IDS.TXT` data file (custom 2023-02-17).
- Updated `regexpu-core` module to version `5.3.1`.

## 4.4.0

- Revamped list of sets of confusable CJK characters, defining five main categories: `duplicates`, `unifiable`, `layout`, `radicals`, `other`.
- Updated `IDS.TXT` data file (custom 2023-02-15).
- Updated `BabelStoneHanPUA.woff2` font file (v1.333).

## 4.3.0

- Improved list of sets of confusable CJK characters.
- Updated `IDS.TXT` data file (custom 2023-02-12).

## 4.2.0

- Expanded list of sets of confusable CJK characters (still WIP).
- Updated `IDS.TXT` data file (2023-02-08).
- Updated `regexpu-core` module to version `5.3.0`.
- Updated `BabelStoneHanPUA.woff2` font file (v1.332).

## 4.1.0

- Improved and expanded list of sets of confusable CJK characters.
- Updated `IDS.TXT` data file (2023-02-06).

## 4.0.0

- Added a new **CJK Confusables** utility (making use of *experimental* data).
- Updated `IDS.TXT` data file (2023-02-02).
- Updated `BabelStoneHanPUA.woff2` font file (v1.331).

## 3.23.0

- Reorganized list of sets of confusable CJK characters (still WIP).
- Updated `BabelStoneHanPUA.woff2` font file (v1.328).
- Updated `IDS.TXT` data file (2023-01-28).

## 3.22.0

- Expanded list of sets of confusable CJK characters.
- Updated `BabelStoneHanPUA.woff2` font file (v1.327).
- Updated `IDS.TXT` data file (2023-01-23).

## 3.21.0

- Drafted list of sets of confusable CJK characters.
- Updated `BabelStoneHanPUA.woff2` font file (v1.325).
- Updated `IDS.TXT` data file (2023-01-19).

## 3.20.0

- Updated `IDS.TXT` data file (2023-01-16).
- Drafted list of equivalent components.

## 3.19.0

- Added one KP-source glyph (CJK Extension H) to the samples of the **CJK Sources** utility.
- Updated `IDS.TXT` data file (2023-01-09).
- Updated `BabelStoneHanPUA.woff2` font file (v1.324).

## 3.18.0

- Updated `IDS.TXT` data file (2023-01-05).
- Updated `BabelStoneHanPUA.woff2` font file (v1.321).

## 3.17.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v15.0.5).
- Updated `IDS.TXT` data file (2023-01-01).
- Updated `BabelStoneHanPUA.woff2` font file (v1.320).
- Updated copyright years.

## 3.16.0

- Updated `IDS.TXT` data file (2022-12-21).

## 3.15.0

- Added progress message to the **CJK Local Fonts** utility while getting the full list of local fonts for the first time.
- Updated `IDS.TXT` data file (2022-12-11).
- Updated `@electron/remote` module to version `2.0.9`.

## 3.14.0

- Updated `IDS.TXT` data file (2022-12-10).
- Updated `BabelStoneHanPUA.woff2` font file (v1.319).

## 3.13.0

- Maximized glyph clickable area for `Copy` contextual menus in SVG graphs of the **CJK Components** utility.
- Updated `IDS.TXT` data file (2022-12-05).
- Updated `BabelStoneHanPUA.woff2` font file (v1.318).

## 3.12.0

- Added `exact match` visual outline to IDS search results in the **Match IDS** feature of the **CJK Components** utility.

## 3.11.1

- Fixed a bug in the **Match IDS** feature of the **CJK Components** utility: some unified CJK characters new in `Unicode 15.0` could be missed when performing a search with the `Nested Match` option enabled.

## 3.11.0

- Adjusted display size of SVG glyphs for V-sources and UTC-sources in the **CJK Sources** utility.
- Updated `IDS.TXT` data file (2022-11-28).
- Updated `BabelStoneHanPUA.woff2` font file (v1.317).

## 3.10.0

- Updated `IDS.TXT` data file (2022-11-24).

## 3.9.0

- Refactored code validating IDS "virtual" sources.
- Improved styling of IDS sources in the **Look Up IDS** feature of the **CJK Components** utility.
- Updated `IDS.TXT` data file (2022-11-16).

## 3.8.0

- Added `East Asian Variant` drop-down menu to the **CJK Local Fonts** utility.
- Updated `regexpu-core` module to version `5.2.2`.
- Updated `IDS.TXT` data file (2022-11-15).

## 3.7.0

- Allowed radical lookup in the **CJK Local Fonts** utility.

## 3.6.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v15.0.4).
- Updated `IDS.TXT` data file (2022-11-11).
- Updated `BabelStoneHanPUA.woff2` font file (v1.316).

## 3.5.0

- Updated reference to the archived UCS2003 code chart for legacy purposes.
- Updated `IDS.TXT` data file (2022-11-04).

## 3.4.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility, based on the latest `BabelStone Han` font (v15.0.3).
- Updated `IDS.TXT` data file (2022-10-26).
- Updated `BabelStoneHanPUA.woff2` font file (v1.315).

## 3.3.0

- Refactored Unicode and Unihan input validation code.
- Updated `IDS.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file.

## 3.2.0

- Added a `Combined Layout` user option to the **CJK Variations** utility.
- Added `Copy` contextual menus to glyphs of graphs in the **CJK Components** utility.
- Fixed reference links and improved tooltips of glyphs in the **CJK Sources** and **CJK Variations** utilities.
- Updated `IDS.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file.

## 3.1.0

- Added new `Copy` contextual menus to the glyphs of the **CJK Sources** and **CJK Variations** utilities.
- Added samples from the new block `CJK Unified Ideographs Extension H` (`Unicode 15.0`) to the **CJK Sources** utility.
- Updated `IDS.TXT` data file.

## 3.0.0

- Added support for `Unicode 15.0`.
- Updated `IDS.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `Electron` to version `17.4.11`.
- Updated `Electron Packager` to version `15.5.2`.

## 2.7.0

- Added a `Notes` field with clickable character links below the ideographic description sequences of the **Look Up IDS** feature of the **CJK Components** utility.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `17.4.8`.

## 2.6.0

- Updated the **unregistered** `BabelStone Collection` in the **CJK Variations** utility.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `17.4.7`.

## 2.5.0

- Updated CJK sample scripts in the **JavaScript Runner** utility.
- Updated `IDS.TXT` and `IDS_PUA.TXT` data files.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `Electron` to version `17.4.6`.

## 2.4.0

- Added new sample script: `Create CJK Glyphs Diff HTML Files` to the **JavaScript Runner** utility.
- Updated `Changes (Undocumented)` samples in the **CJK Sources** utility.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `17.4.5`.

## 2.3.0

- Allowed `GlyphWiki` hex syntax for IVS input in the **CJK Local Fonts** utility.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `17.4.3`.
- Updated `Electron Packager` to version `15.5.1`.

## 2.2.0

- Added new interactive feature to the **CJK Variations** utility: visual feedback on mouse click to spot differences in glyph variations.
- Added `Other Applications` to `Help` menu.
- Updated `IDS.TXT` data file.
- Updated `Electron Packager` to version `15.5.0`.

## 2.1.0

- Added **unregistered** `BabelStone Collection` as an *experimental* feature to the **CJK Variations** utility.
- Updated `IDS.TXT` data file.

## 2.0.0

- Added new **CJK Variations** utility.
- Added new IVD sample script and updated CJK sample scripts in the **JavaScript Runner** utility.
- Fixed excess graphemes issue in the **Parse IDS** feature of the **CJK Components** utility.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `17.4.0`.

## 1.13.0

- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `17.3.0`.

## 1.12.0

- Updated instructions of the **Parse IDS** feature of the **CJK Components** utility.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `IDS_PUA.TXT` data file.
- Updated `IDS.TXT` data file.
- Added support for building macOS universal binaries (`x64` and `arm64`).
- Updated `@electron/remote` module to version `2.0.8`.

## 1.11.0

- Added support for Unicode Variation Sequences to the **CJK Components** utility.
- Added new samples to the **Parse IDS** feature of the **CJK Components** utility: unencoded entries, standardized variant sequences, ideographic variation sequences (registered and unregistered), Japanese square forms, Vietnamese ligatures (experimental).
- Added standardized variation sequence to tooltip of Unihan compatibility characters.
- Improved performance of built-in functions `$.write ()` and `$.writeln ()` in the **JavaScript Runner** utility.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `Electron` to version `17.1.2`.
- Updated `@electron/remote` module to version `2.0.7`.

## 1.10.0

- Added a dashed outline to each frame of CJK characters whose glyph is different with or without a VS (Variation Selector) and added momentary diff visualization by alt-click (or shift-click) to the **CJK Local Fonts** utility.
- Updated `IDS.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `Electron` to version `17.0.1`.

## 1.9.0

- Added font name filter and optional variation selector to the **CJK Local Fonts** utility.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `IDS.TXT` data file.

## 1.8.0

- Updated `Glyph Defects` samples in the **CJK Sources** utility.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `@electron/remote` module to `2.0.4`.
- Updated `Electron` to version `17.0.0`.

## 1.7.1

- Improved filtering of "blank" CJK glyphs for some "ill-behaved" fonts in the **CJK Local Fonts** utility.
- Updated reference links of the **CJK Local Fonts** utility.

## 1.7.0

- Added new **CJK Local Fonts** utility.
- Updated `IDS.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `regexpu-core` module to `5.0.1`.

## 1.6.2

- Fixed display of "overshooting" V-source glyphs being slightly cropped at top and bottom in the **CJK Sources** utility.
- Allowed lowercase 'u' prefix for "relaxed" hex code point formats.
- Updated `Electron` to version `16.0.7`.

## 1.6.1

- Updated `IDS.TXT` data file.
- Updated `BabelStoneHanPUA.woff2` font file.
- Updated `Electron` to version `16.0.6`.

## 1.6.0

- Fixed missing text cursor over selectable IDS text in the **CJK Components** utility.
- Updated CJK source samples and instructions in the **CJK Sources** utility.
- Updated copyright years.

## 1.5.5

- Improved performance of table creation in the **CJK Sources** utility.
- Updated `Electron` to version `16.0.5`.

## 1.5.4

- Used Windows-compatible end-of-line markers when parsing data text files (BUG FIX).
- Reduced letter spacing of alphanumeric source codes in the **CJK Sources** utility.

## 1.5.3

- Updated reference links.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `16.0.4`.

## 1.5.2

- Improved user interface of the **Look Up IDS** feature of the **CJK Components** utility.
- Updated reference links.
- Updated screenshots.

## 1.5.1

- Added "backtracking" link to current entry in graph view of the **Look Up IDS** feature of the **CJK Components** utility.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `16.0.1`.

## 1.5.0

- Used larger entries for graphs in the **CJK Components** utility.
- Updated `IDS.TXT` data file.
- Updated `Electron` to version `16.0.0`.

## 1.4.2

- Improved the Glyphs Statistics table of the **CJK Sources** utility.
- Updated samples and reference links of the **CJK Sources** utility.

## 1.4.1

- Added components linking in graph view of the **Look Up IDS** feature of the **CJK Components** utility.
- Updated `Electron` to version `15.3.1`.

## 1.4.0

- Added a `Show Graphs` checkbox to the **Look Up IDS** feature of the **CJK Components** utility.

## 1.3.0

- Added CJK sample script to the **JavaScript Runner** utility.
- Added new built-in function `$.getpath ()` to the **JavaScript Runner** utility.

## 1.2.1

- Reordered `UTC-Source` in the **CJK Sources** utility.
- Updated samples.
- Updated reference links.

## 1.2.0

- Added Glyphs Statistics table to the **CJK Sources** utility.
- Updated samples.
- Updated reference links.

## 1.1.0

- Fixed possible PUA font issue.
- Reorganized code hierarchy.
- Updated instructions.
- Updated `Electron` to version `15.3.0`.

## 1.0.0

- Initial release.

