# <img src="icons/icon-256.png" width="64px" align="center" alt="Unicopedia Sinica icon"> UNICOPEDIA SINICA

**Unicopedia Sinica** is a developer-oriented set of Unicode utilities related to ideographs, wrapped into one single app, built with [Electron](https://www.electronjs.org/).

This desktop application works on macOS, Linux and Windows operating systems.

It is a specialized complement to the [Unicopedia Plus](https://codeberg.org/tonton-pixel/unicopedia-plus) application.

<img src="screenshots/US-social-preview.png" alt="Unicopedia Sinica social preview">

## Utilities

The following utilities are currently available:

- [**CJK Components**](#cjk-components)
    - [**Look Up IDS**](#look-up-ids)
    - [**Parse IDS**](#parse-ids)
    - [**Match IDS**](#match-ids)
    - [**Find by Components**](#find-by-components)
- [**CJK Local Fonts**](#cjk-local-fonts)
- [**CJK Radicals**](#cjk-radicals)
- [**CJK Related**](#cjk-related)
- [**CJK Sources**](#cjk-sources)
- [**CJK Strokes**](#cjk-strokes)
- [**CJK Variations**](#cjk-variations)
- [**JavaScript Runner**](#javascript-runner)
- [**Pan-CJK Font Variants**](#pan-cjk-font-variants)

## CJK Components

### Look Up IDS

- The **Look Up IDS** feature of the **CJK Components** utility displays all the IDS (Ideographic Description Sequences) of a given Unihan character, as compiled in a custom fork of the [IDS.TXT](https://www.babelstone.co.uk/CJK/IDS.HTML) data file, maintained by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
- Any Unihan character can be entered in the <kbd>Unihan</kbd> input field either as a character or a code point. Click on the <kbd>Look Up</kbd> button to display the IDS.
- Previously looked up characters are kept in a history stack; use the <kbd>Alt</kbd>+<kbd>↑</kbd> and <kbd>Alt</kbd>+<kbd>↓</kbd> keyboard shortcuts to navigate through them up and down inside the input field. Alternatively, you can also use the <kbd>Lookup&nbsp;History&nbsp;▾</kbd> pop-up menu to automatically look up a specific character.
- Click on the <kbd>Show Graphs</kbd> checkbox to display the IDS as graphs instead of text strings.
- IDS are provided for the set of 97,680 Unihan characters (excluding CJK *compatibility* ideographs) defined in **Unicode 16.0**.
- The various sources are represented by a single letter:
    | Designation | Alias      | Source      |
    | ----------- | ---------- | ----------- |
    | G           | G-Source   | China       |
    | H           | H-Source   | Hong Kong   |
    | M           | M-Source   | Macao       |
    | T           | T-Source   | Taiwan      |
    | J           | J-Source   | Japan       |
    | K           | K-Source   | South Korea |
    | P           | KP-Source  | North Korea |
    | V           | V-Source   | Vietnam     |
    | U           | UTC-Source | UTC         |
    | S           | SAT-Source | SAT         |
    | B           | UK-Source  | U.K.        |
    | Q           | UCS2003    | Legacy      |
    | X           | (Alternative IDS)        |
    | Z           | (Unifiable Variant)      |
- Notes:
    - Unencoded components of IDS, if any, are assigned code points belonging to the PUA (Private Use Area) block, and are properly displayed by using an embedded copy of the custom font [BabelStone Han PUA](https://www.babelstone.co.uk/Fonts/PUA.html), created by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
    - For best display results, most recent versions of the following fonts should be downloaded and installed at the OS level:
        - [BabelStone Han](https://www.babelstone.co.uk/Fonts/Han.html) (`BabelStoneHan.ttf`)
        - [Jigmo](https://kamichikoichi.github.io/jigmo/) (`Jigmo.ttf`, `Jigmo2.ttf`, `Jigmo3.ttf`)
        - [TH-Tshyn](http://cheonhyeong.com/Simplified/download.html) (`TH-Tshyn-P0.ttf`, `TH-Tshyn-P1.ttf`, `TH-Tshyn-P2.ttf`, `TH-Tshyn-P16.ttf`)
    - The IDS are based on the **Unicode 16.0** reference character glyphs shown in the [CodeCharts.pdf](https://www.unicode.org/Public/16.0.0/charts/CodeCharts.pdf) document.

<img src="screenshots/cjk-components-lookup-ids.png" width="1080px" alt="CJK Components - Look Up IDS screenshot">

<img src="screenshots/cjk-components-lookup-ids-show-graphs.png" width="1080px" alt="CJK Components - Look Up IDS - Show Graphs screenshot">

### Parse IDS

- The **Parse IDS** feature of the **CJK Components** utility displays the parsing graph of any well-formed IDS (Ideographic Description Sequence), in accordance with the extended set of IDCs (Ideographic Description Characters) defined in **Unicode 16.0**.
- The IDS string can by directly typed, or pasted from the clipboard into the <kbd>IDS</kbd> input field.
- Optionally, a Unihan character can be used as reference in the <kbd>Entry</kbd> input field. Any standardized variant or Ideographic Variation Sequence (IVS) is also accepted, and is displayed in the graph with an outstanding dashed outline.
- It is possible to input predefined sets of Entry and IDS strings selected from the <kbd>Samples&nbsp;▾</kbd> pop-up menu.
- As a convenience, the input fields can be emptied using the <kbd>Clear</kbd> button.
- The IDS graph can be displayed either vertically or horizontally. Use the <kbd>Display Mode</kbd> drop-down menu to toggle between the two modes.
- Notes:
    - This feature is primarily designed for IDS validation (well-formed *syntax*), and can even be used as a kind of "playground" for experimentation, without the need to provide any correct *semantics*.
    - The IDS parsing is performed recursively, based on the following set of *prefix* operators:
        | Operator | Name                            | Arity |
        | -------- | ------------------------------- | ----- |
        | ⿰        | IDC Left to Right               | 2     |
        | ⿱        | IDC Above to Below              | 2     |
        | ⿲        | IDC Left to Middle and Right    | 3     |
        | ⿳        | IDC Above to Middle and Below   | 3     |
        | ⿴        | IDC Full Surround               | 2     |
        | ⿵        | IDC Surround from Above         | 2     |
        | ⿶        | IDC Surround from Below         | 2     |
        | ⿷        | IDC Surround from Left          | 2     |
        | ⿸        | IDC Surround from Upper Left    | 2     |
        | ⿹        | IDC Surround from Upper Right   | 2     |
        | ⿺        | IDC Surround from Lower Left    | 2     |
        | ⿻        | IDC Overlaid                    | 2     |
        | ⿼        | IDC Surround from Right         | 2     |
        | ⿽        | IDC Surround from Lower Right   | 2     |
        | ⿾        | IDC Horizontal Reflection       | 1     |
        | ⿿        | IDC Rotation                    | 1     |
        | ㇯        | IDC Subtraction                 | 2     |
        | 〾        | Ideographic Variation Indicator | 1     |
    - Components whose code point belongs to the PUA (Private Use Area) block are displayed by using an embedded copy of the custom font [BabelStone Han PUA](https://www.babelstone.co.uk/Fonts/PUA.html), created by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
    - For best display results, most recent versions of the following fonts should be downloaded and installed at the OS level:
        - [BabelStone Han](https://www.babelstone.co.uk/Fonts/Han.html) (`BabelStoneHan.ttf`)
        - [Jigmo](https://kamichikoichi.github.io/jigmo/) (`Jigmo.ttf`, `Jigmo2.ttf`, `Jigmo3.ttf`)
        - [TH-Tshyn](http://cheonhyeong.com/Simplified/download.html) (`TH-Tshyn-P0.ttf`, `TH-Tshyn-P1.ttf`, `TH-Tshyn-P2.ttf`, `TH-Tshyn-P16.ttf`)

<img src="screenshots/cjk-components-parse-ids-ivs.png" width="1080px" alt="CJK Components - Parse IDS (IVS) screenshot">

<img src="screenshots/cjk-components-parse-ids-unencoded.png" width="1080px" alt="CJK Components - Parse IDS (Unencoded) screenshot">

### Match IDS

- The **Match IDS** feature of the **CJK Components** utility displays a list of IDS-matching Unihan characters, including through regular expressions. It makes use of the IDS (Ideographic Description Sequences) defined in a custom fork of the [IDS.TXT](https://www.babelstone.co.uk/CJK/IDS.HTML) data file, maintained by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
- After entering a query, click on the <kbd>Search</kbd> button to display a list of all relevant matches, if any, ordered by code point value.
- Click on the <kbd>Nested Match</kbd> toggle button to extend the search to IDS-nested characters whose *IDS* match the query string.
- Click on the <kbd>Code Points</kbd> checkbox to display the code point under each matching Unihan character.
- It is possible to choose how many characters are shown one page at a time.
- The search is performed on the set of 97,680 Unihan characters (excluding CJK *compatibility* ideographs) defined in **Unicode 16.0**.
- The results may include the searched component itself when it happens to be a proper Unihan character too.
- Use the <kbd>Search Results&nbsp;▾</kbd> pop-up menu to perform an action among:
    - `Copy Search Results` [copy the search results as string to the clipboard]
    - `Save Search Results...` [save the search results as string to a text file]
    - `Clear Search Results` [clear the current list of search results]
- Use the <kbd>Sorted by</kbd> drop-down menu to select how results get sorted, among: `Code Points`, `Total Strokes`, and `Radical-Strokes`.
- Various examples of regular expressions are provided for quick copy-and-paste.
- Notes:
    - Unencoded components of IDS, if any, are assigned code points belonging to the PUA (Private Use Area) block, and are properly displayed by using an embedded copy of the custom font [BabelStone Han PUA](https://www.babelstone.co.uk/Fonts/PUA.html), created by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
    - For best display results, most recent versions of the following fonts should be downloaded and installed at the OS level:
        - [BabelStone Han](https://www.babelstone.co.uk/Fonts/Han.html) (`BabelStoneHan.ttf`)
        - [Jigmo](https://kamichikoichi.github.io/jigmo/) (`Jigmo.ttf`, `Jigmo2.ttf`, `Jigmo3.ttf`)
        - [TH-Tshyn](http://cheonhyeong.com/Simplified/download.html) (`TH-Tshyn-P0.ttf`, `TH-Tshyn-P1.ttf`, `TH-Tshyn-P2.ttf`, `TH-Tshyn-P16.ttf`)
    - The IDS are based on the **Unicode 16.0** reference character glyphs shown in the [CodeCharts.pdf](https://www.unicode.org/Public/16.0.0/charts/CodeCharts.pdf) document.

<img src="screenshots/cjk-components-match-ids.png" width="1080px" alt="CJK Components - Match IDS screenshot">

### Find by Components

___\*\* Under Construction \*\*___

- The **Find by Components** feature of the **CJK Components** utility displays a list of Unihan characters matching a set of individual components. It makes use of the IDS (Ideographic Description Sequences) defined in a custom fork of the [IDS.TXT](https://www.babelstone.co.uk/CJK/IDS.HTML) data file, maintained by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
- After entering a query, click on the <kbd>Search</kbd> button to display a list of all relevant matches, if any, ordered by code point value.
- Click on the <kbd>Code Points</kbd> checkbox to display the code point under each matching Unihan character.
- It is possible to choose how many characters are shown one page at a time.
- The search is performed on the set of 97,680 Unihan characters (excluding CJK *compatibility* ideographs) defined in **Unicode 16.0**.
- Use the <kbd>Search Results&nbsp;▾</kbd> pop-up menu to perform an action among:
    - `Copy Search Results` [copy the search results as string to the clipboard]
    - `Save Search Results...` [save the search results as string to a text file]
    - `Clear Search Results` [clear the current list of search results]
- Notes:
    - Unencoded components of IDS, if any, are assigned code points belonging to the PUA (Private Use Area) block, and are properly displayed by using an embedded copy of the custom font [BabelStone Han PUA](https://www.babelstone.co.uk/Fonts/PUA.html), created by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
    - For best display results, most recent versions of the following fonts should be downloaded and installed at the OS level:
        - [BabelStone Han](https://www.babelstone.co.uk/Fonts/Han.html) (`BabelStoneHan.ttf`)
        - [Jigmo](https://kamichikoichi.github.io/jigmo/) (`Jigmo.ttf`, `Jigmo2.ttf`, `Jigmo3.ttf`)
        - [TH-Tshyn](http://cheonhyeong.com/Simplified/download.html) (`TH-Tshyn-P0.ttf`, `TH-Tshyn-P1.ttf`, `TH-Tshyn-P2.ttf`, `TH-Tshyn-P16.ttf`)
    - The IDS are based on the **Unicode 16.0** reference character glyphs shown in the [CodeCharts.pdf](https://www.unicode.org/Public/16.0.0/charts/CodeCharts.pdf) document.

<img src="screenshots/cjk-components-find-by-components.png" width="1080px" alt="CJK Components - Find by Components screenshot">

## CJK Local Fonts

- The **CJK Local Fonts** utility displays all the local font glyphs of a given Unihan character.
- Any Unihan character can be entered in the <kbd>Unihan</kbd> input field either as a character or a code point. Click on the <kbd>Look Up</kbd> button to display all the glyphs.
- Standardized variants and Ideographic Variation Sequences (IVS) are also accepted in input, either directly, e.g., `劍󠄁`, or as a combination of two code points: Unihan base character + variation selector (VS1 to VS256), e.g., `U+6F22 U+FE00`, or `U+9F8D U+E0101`; the specific formats `<9F8D, E0107>` *(IVD)* or `u9f8d-ue010e` *(GlyphWiki)* are also supported.
- Use the <kbd>VS</kbd> drop-down menu to add, replace, or remove a variation selector (`1` to `256`, or `--` for none).
- Previously looked up characters are kept in a history stack; use the <kbd>Alt</kbd>+<kbd>↑</kbd> and <kbd>Alt</kbd>+<kbd>↓</kbd> keyboard shortcuts to navigate through them up and down inside the input field. Alternatively, you can also use the <kbd>Lookup&nbsp;History&nbsp;▾</kbd> pop-up menu to automatically look up a specific character.
- Click on the <kbd>Compact Layout</kbd> checkbox to display the local font glyphs in a more compact way: hovering over each glyph frame brings up a tooltip with the local font name.
- Use the <kbd>East Asian Variant</kbd> drop-down menu to display specific logographic glyph variants.
- Use the <kbd>Font Name Filter</kbd> input field to restrict in real time the display of local font glyphs to the font names matching the text string.
- Notes:
    - A dashed outline is added to a character frame whenever the glyph of a Unihan character coming with a variation selector is visually different from the glyph of its base character alone; in such case, alt-clicking (or shift-clicking) inside the character frame displays momentarily the base character glyph; this is especially useful to spot subtle differences between glyph variations.
    - For best coverage of Unicode Variation Sequences, some of the following fonts should be downloaded and installed at the OS level:
        - [Jigmo](https://kamichikoichi.github.io/jigmo/) (`Jigmo.ttf`, `Jigmo2.ttf`, `Jigmo3.ttf`)
        - [IPA MJ Mincho](https://moji.or.jp/mojikiban/font/) (`ipamjm.ttf`)
        - [IPAEx Font Ver.004.01](https://moji.or.jp/ipafont/ipaex00401/) (`ipaexm.ttf`, `ipaexg.ttf`)
        - [Source Han Serif](https://github.com/adobe-fonts/source-han-serif/tree/release) (various set formats)
        - [Source Han Sans](https://github.com/adobe-fonts/source-han-sans/tree/release) (various set formats)
    - Radicals do *not* belong to the Unihan character set; they are allowed to be looked up here merely as a convenience, since they are closely related, and because their appearance is similar, or even identical, to their equivalent CJK unified ideograph.
    - Likewise, individual CJK strokes are also allowed to be looked up.

<img src="screenshots/cjk-local-fonts.png" width="1080px" alt="CJK Local Fonts screenshot">

<img src="screenshots/cjk-local-fonts-filter.png" width="1080px" alt="CJK Local Fonts - Filter screenshot">

<img src="screenshots/cjk-local-fonts-vs.png" width="1080px" alt="CJK Local Fonts - Variation Selector screenshot">

<img src="screenshots/cjk-local-fonts-eav.png" width="1080px" alt="CJK Local Fonts - East Asian Variant screenshot">

<img src="screenshots/cjk-local-fonts-radical.png" width="1080px" alt="CJK Local Fonts - Radical screenshot">

<img src="screenshots/cjk-local-fonts-stroke.png" width="1080px" alt="CJK Local Fonts - Stroke screenshot">

## CJK Radicals

- The **CJK Radicals** utility displays visual information about the 329 (214 + 115) Chinese radical characters defined in **Unicode 16.0**.
- For each KangXi radical (i.e., belonging to the `Kangxi Radicals` block), related CJK radicals (i.e., belonging to the `CJK Radicals Supplement` block), if any, are listed in the same group indexed by radical number, from `1` to `214`.
- Use the <kbd>Live Search…</kbd> input field to control which radicals are listed. As you type in the search field, only groups of radicals whose data fields match the input string get displayed:
    <table class="fields-table">
    <tr><th>Data Field</th><th>Description</th></tr>
    <tr><td>Number</td><td>Radical number (1 to 214)</td></tr>
    <tr><td>Full Name</td><td>Radical full name</td></tr>
    <tr><td>Code Point</td><td>Radical code point</td></tr>
    <tr><td>Radical</td><td>Code chart representative glyph</td></tr>
    <tr><td>Local Font</td><td>Default local glyph rendering</td></tr>
    <tr><td>Eq. Unified</td><td>Equivalent CJK unified ideograph</td></tr>
    <tr><td>Normalized</td><td>Normalized radical (NFKC/NFKD)</td></tr>
    <tr><td>Other</td><td>Substitute/Alternate</td></tr>
    </table>
- Click on the <kbd>Whole Word</kbd> checkbox to further restrict the search to whole words only.
- Notes:
    - The `CJK Radical Repeat` is considered a variant form of the `KangXi Radical Ice` (*not* `KangXi Radical Dot` *or* `KangXi Radical Two`).
    - To display the representative glyphs of the radicals as they appear in the code charts, this utility makes use of an embedded copy of the [CJK Radicals](https://github.com/adobe-fonts/cjk-radicals) font.
    - The `Eq. Unified` field uses information contained in the [EquivalentUnifiedIdeograph.txt](https://www.unicode.org/Public/16.0.0/ucd/EquivalentUnifiedIdeograph.txt) and [CJKRadicals.txt](https://www.unicode.org/Public/16.0.0/ucd/CJKRadicals.txt) data files.

<img src="screenshots/cjk-radicals.png" width="1080px" alt="CJK Radicals screenshot">

<img src="screenshots/cjk-radicals-live-search.png" width="1080px" alt="CJK Radicals - Live Search screenshot">

<img src="screenshots/cjk-radicals-virtual-radical.png" width="1080px" alt="CJK Radicals - Virtual Radical screenshot">

<img src="screenshots/cjk-radicals-normalization-issues.png" width="1080px" alt="CJK Radicals - Normalization Issues screenshot">

<img src="screenshots/cjk-radicals-other.png" width="1080px" alt="CJK Radicals - Other screenshot">

## CJK Related

- The **CJK Related** utility displays sets of related CJK characters as a *linear* list view, followed by a *detailed* list view made of specific categories, among: `Addition`, `Confusable`, `Duplicate`, `Layout`, `Not-Unifiable`, `Parts`, `Radical`, `Related`, `Repeated`, `Series`, `Shinjitai-Kyūjitai`, `Simplified-Traditional`, `Substitution`, `Unifiable`.
    | Category | Description | Notes |
    | -------- | ----------- | ----- |
    | **Addition** | Additional component to CJK characters | Ordered by stroke count |
    | **Confusable** | Confusable CJK characters | Visually confusable characters |
    | **Duplicate** | Duplicate CJK characters | Characters sharing a common IDS |
    | **Layout** | CJK characters with related layout | Same components but different positioning |
    | **Not-Unifiable** | Not unifiable CJK characters | IRG-provided NUCV lists |
    | **Parts** | Parts of CJK characters | Ordered by stroke count |
    | **Radical** | Equivalent CJK characters of CJK radicals | KangXi radical followed by CJK radicals |
    | **Related** | Related CJK characters | All-purpose ("catch-all") category |
    | **Repeated** | Repeated components of CJK characters | Ordered by repetition factor |
    | **Series** | Series of CJK characters | Ordered logically |
    | **Shinjitai-Kyūjitai** | Japanese simplified and traditional CJK characters | Ordered by stroke count |
    | **Simplified-Traditional** | Chinese simplified and traditional CJK characters | Ordered by stroke count |
    | **Substitution** | Substituted component between CJK characters | Ordered by stroke count |
    | **Unifiable** | Unifiable CJK characters | IRG-provided UCV lists extensions |
- The term *related* is to be taken in a broad sense: *duplicate*, *mistaken*, *spoofing*, *look-alike*, visually *related*, regional *variants*, *series*, or *confusable* (intentionally or not) CJK characters...
- Sets of Chinese simplified/traditional CJK characters are not limited to the ones *registered* in the Unihan database; they may contain additional *extrapolated* sets, sharing the same component substitution pattern, including *hybrid* combinations of simplified and traditional components.
- Likewise, although sets of Japanese shinjitai/kyūjitai CJK characters are generally not *rule-based*, they may be *extrapolated* by following plausible substitution patterns, and considered related as well.
- Any Unihan character (unified or compatibility) can be entered in the <kbd>Unihan</kbd> input field either as a character or a code point. Click on the <kbd>Look Up</kbd> button to display the looked up character followed by a list of related characters, if any, sorted by total stroke count in the *linear* list view.
- Previously looked up characters are kept in a history stack; use the <kbd>Alt</kbd>+<kbd>↑</kbd> and <kbd>Alt</kbd>+<kbd>↓</kbd> keyboard shortcuts to navigate through them up and down inside the input field. Alternatively, you can also use the <kbd>Lookup&nbsp;History&nbsp;▾</kbd> pop-up menu to automatically look up a specific character.
- Click on the <kbd>Extrapolated&nbsp;(*)</kbd> checkbox to include the *extrapolated* Chinese simplified/traditional and Japanese shinjitai/kyūjitai sets as well; otherwise, only the sets registered in the Unihan database are listed.
- For the sake of precision, sub-categories and group names can make use of unencoded components of IDS, which are properly displayed by using an embedded copy of the custom font [BabelStone Han PUA](https://www.babelstone.co.uk/Fonts/PUA.html), created by [Andrew West](https://en.wikipedia.org/wiki/Andrew_West_(linguist)).
- For best display results, most recent versions of the following fonts should be downloaded and installed at the OS level:
    - [BabelStone Han](https://www.babelstone.co.uk/Fonts/Han.html) (`BabelStoneHan.ttf`)
    - [Jigmo](https://kamichikoichi.github.io/jigmo/) (`Jigmo.ttf`, `Jigmo2.ttf`, `Jigmo3.ttf`)
    - [TH-Tshyn](http://cheonhyeong.com/Simplified/download.html) (`TH-Tshyn-P0.ttf`, `TH-Tshyn-P1.ttf`, `TH-Tshyn-P2.ttf`, `TH-Tshyn-P16.ttf`)
- Note: code points of CJK *compatibility* characters are systematically displayed in *italics*.

<img src="screenshots/cjk-related.png" width="1080px" alt="CJK Related screenshot">

## CJK Sources

- The **CJK Sources** utility displays in a grid fashion the various sources of a given subset of CJKV (Chinese/Japanese/Korean/Vietnamese) characters, as referenced in the **Unicode 16.0** [CodeCharts.pdf](https://www.unicode.org/Public/16.0.0/charts/CodeCharts.pdf) document.
- This is especially useful for comparison purposes between relatable character glyphs.
- These CJKV characters belong to the full set of 98,682 Unihan characters (including CJK *compatibility* ideographs) defined in **Unicode 16.0**.
- Although *deprecated* since Unicode 14.0, the glyphs corresponding to the UCS2003 source are still listed here, but only for legacy purposes; they are referenced in the archived code chart: [CJK Unified Ideographs Extension B - UCS2003.pdf](https://www.unicode.org/Public/13.0.0/charts/UCS2003.pdf).</li>
- For best display results, most recent versions of the following fonts should be downloaded and installed at the OS level:
    - [BabelStone Han](https://www.babelstone.co.uk/Fonts/Han.html) (`BabelStoneHan.ttf`)
    - [Jigmo](https://kamichikoichi.github.io/jigmo/) (`Jigmo.ttf`, `Jigmo2.ttf`, `Jigmo3.ttf`)
    - [TH-Tshyn](http://cheonhyeong.com/Simplified/download.html) (`TH-Tshyn-P0.ttf`, `TH-Tshyn-P1.ttf`, `TH-Tshyn-P2.ttf`, `TH-Tshyn-P16.ttf`)
- CJK characters can be entered either directly in the "Characters" input field, or using a series of code points in hexadecimal format in the "Code points" input field.
- It is also possible to input predefined strings of CJK characters selected from the <kbd>Samples&nbsp;▾</kbd> pop-up menu.
- As a convenience, the input fields can be emptied using the <kbd>Clear</kbd> button.
In output, the standard Unicode code point format `U+9999` is used, i.e. "U+" directly followed by 4 or 5 hex digits.
- In input, more hexadecimal formats are allowed, including Unicode escape sequences, such as `\u6E7E` or `\u{21FE7}`. Moving out of the field or typing the Enter key converts all valid codes to standard Unicode code point format.
- Code point and alphanumeric source references of CJK *compatibility* characters are systematically displayed in *italics*.
- Whereas the original code charts are making use of mutually incompatible, block-specific source orderings, this utility displays the relevant sources always sorted in the same order, discarding any empty column for the sake of clarity:
    | Prefix | Source         | Unihan Property |
    | ------ | -------------- | --------------- |
    | G      | China          | kIRG_GSource    |
    | H      | Hong Kong      | kIRG_HSource    |
    | M      | Macao          | kIRG_MSource    |
    | T      | Taiwan         | kIRG_TSource    |
    | J      | Japan          | kIRG_JSource    |
    | K      | South Korea    | kIRG_KSource    |
    | KP     | North Korea    | kIRG_KPSource   |
    | V      | Vietnam        | kIRG_VSource    |
    | UTC    | UTC            | kIRG_USource    |
    | SAT    | SAT            | kIRG_SSource    |
    | UK     | U.K.           | kIRG_UKSource   |
    | UCS    | UCS2003&nbsp;† | *&lt;none&gt;*  |
- UTC stands for *Unicode Technical Committee*, which is responsible for the development and maintenance of the Unicode Standard.
- SAT (*SAmganikikrtam Taisotripitakam* in Sanskrit) represents a machine-readable text database of the [Taishō Tripiṭaka](https://en.wikipedia.org/wiki/Taish%C5%8D_Tripi%E1%B9%ADaka).
- Tables of glyphs statistics and IRG source references are available for quick reference.

<img src="screenshots/cjk-sources.png" width="1080px" alt="CJK Sources screenshot">

## CJK Strokes

- The **CJK Strokes** utility displays visual information about the 38 individual CJK strokes defined in **Unicode 16.0**.
- Use the <kbd>Live Search…</kbd> input field to control which CJK strokes are listed. As you type in the search field, only CJK strokes whose data fields match the input string get displayed:
    <table class="fields-table">
    <tr><th>Data Field</th><th>Description</th></tr>
    <tr><td>Acronym</td><td>Stroke acronym string</td></tr>
    <tr><td>Code Point</td><td>Stroke code point</td></tr>
    <tr><td>Stroke</td><td>Code chart representative glyph</td></tr>
    <tr><td>Local Font</td><td>Default local glyph rendering</td></tr>
    <tr><td>Eq. Unified</td><td>Equivalent CJK unified ideograph</td></tr>
    </table>
- Click on the <kbd>Whole Word</kbd> checkbox to further restrict the search to whole words only.
- Notes:
    - To display the representative glyphs of the strokes as they appear in the code charts, this utility makes use of an embedded copy of the [CJK Symbols](https://github.com/unicode-org/cjk-symbols) font.
    - The `Eq. Unified` field uses information contained in the [EquivalentUnifiedIdeograph.txt](https://www.unicode.org/Public/16.0.0/ucd/EquivalentUnifiedIdeograph.txt) data file.

<img src="screenshots/cjk-strokes.png" width="1080px" alt="CJK Strokes screenshot">

## CJK Variations

- The **CJK Variations** utility displays all the registered Ideographic Variation Sequences (IVS) of a given Unihan character, as referenced in their respective **IVD 2022** charts:
    - [Adobe-Japan1 collection - IVD_Charts_Adobe-Japan1.pdf](https://www.unicode.org/ivd/data/2022-09-13/IVD_Charts_Adobe-Japan1.pdf)
    - [Hanyo-Denshi collection - IVD_Charts_Hanyo-Denshi.pdf](https://www.unicode.org/ivd/data/2022-09-13/IVD_Charts_Hanyo-Denshi.pdf)
    - [KRName collection - IVD_Charts_KRName.pdf](https://www.unicode.org/ivd/data/2022-09-13/IVD_Charts_KRName.pdf)
    - [Moji_Joho collection - IVD_Charts_Moji_Joho.pdf](https://www.unicode.org/ivd/data/2022-09-13/IVD_Charts_Moji_Joho.pdf)
    - [MSARG collection - IVD_Charts_MSARG.pdf](https://www.unicode.org/ivd/data/2022-09-13/IVD_Charts_MSARG.pdf)
- In addition, as an *experimental* feature, an **unregistered** set of [BabelStone Han Variation Sequences](https://www.babelstone.co.uk/Fonts/BSH_IVS.html) is also supported, intended to be registered in the [Ideographic Variation Database](https://www.unicode.org/ivd/) as a future `BabelStone Collection`.
- Click on the <kbd>Combined Layout</kbd> checkbox to display a consolidated table view, where all IVSes sharing the same variation selector value are grouped in the same column.
- Clicking inside any character glyph displays momentarily the same glyph for all variations in the same collection, while alt-clicking (or shift-clicking) applies to all characters glyphs sharing the same IVS across different collections. This is especially useful to quickly spot visual differences between glyph variations.
- Any Unihan character can be entered in the <kbd>Unihan</kbd> input field either as a character or a code point. Click on the <kbd>Look Up</kbd> button to display all the IVS glyphs along with their VS (Variation Selector) code points.
- Previously looked up characters are kept in a history stack; use the <kbd>Alt</kbd>+<kbd>↑</kbd> and <kbd>Alt</kbd>+<kbd>↓</kbd> keyboard shortcuts to navigate through them up and down inside the input field. Alternatively, you can also use the <kbd>Lookup&nbsp;History&nbsp;▾</kbd> pop-up menu to automatically look up a specific character.

<img src="screenshots/cjk-variations.png" width="1080px" alt="CJK Variations screenshot">

<img src="screenshots/cjk-variations-combined.png" width="1080px" alt="CJK Variations - Combined screenshot">

<img src="screenshots/cjk-variations-unregistered.png" width="1080px" alt="CJK Variations (Unregistered) screenshot">

## JavaScript Runner

- The **JavaScript Runner** utility lets you execute JavaScript code, and comes with several sample scripts related to CJK, IDS, and IVD; it is useful for quick testing/prototyping or data processing.

<img src="screenshots/javascript-runner.png" width="1080px" alt="JavaScript Runner screenshot">

## Pan-CJK Font Variants

- The **Pan-CJK Font Variants** utility displays simultaneously any string of CJK (Chinese/Japanese/Korean) characters in five different typefaces belonging to the open-source set of [Source Han Serif Fonts](https://github.com/adobe-fonts/source-han-serif):

| Language | Tag | Code | Typeface |
| -------- | --- | ---- | -------- |
| Japanese | ja | JP | Source Han Serif |
| Korean | ko | KR | Source Han Serif K |
| Simplified Chinese | zh-Hans | SC | Source Han Serif SC |
| Traditional Chinese (Taiwan) | zh-Hant-TW | TC | Source Han Serif TC |
| Traditional Chinese (Hong Kong) | zh-Hant-HK | HK | Source Han Serif HC |

- Additionally, it is possible to specify a set of logographic glyph variants for display by using the <kbd>East Asian Variant</kbd> drop-down menu.
- Font variants of the CJK characters can be visualized either vertically or horizontally. Use the <kbd>Writing Mode</kbd> drop-down menu to toggle between the two modes.
- Clicking inside any character frame displays momentarily the same glyph for all language flavors, while alt-clicking (or shift-clicking) applies to all characters of the string in a given language flavor. This is especially useful to quickly spot the differences between glyph variations. If the <kbd>⇪ Caps Lock</kbd> key is turned on as well, the differences are shown in contrasting colors instead.
- CJK characters can be entered either directly in the "Characters" input field, or using a series of code points in hexadecimal format in the "Code points" input field.
- It is also possible to input predefined strings of CJK characters selected from the <kbd>Samples&nbsp;▾</kbd> pop-up menu; some of them make use of the information found in the [StandardizedVariants.txt](https://www.unicode.org/Public/16.0.0/ucd/StandardizedVariants.txt) or [IVD_Sequences.txt](https://www.unicode.org/ivd/data/2022-09-13/IVD_Sequences.txt) data files.
- As a convenience, the input fields can be emptied using the <kbd>Clear</kbd> button.
- In output, the standard Unicode code point format `U+7ADC` is used, i.e. "U+" directly followed by 4 or 5 hex digits.
- In input, more hexadecimal formats are allowed, including Unicode escape sequences, such as `\u9F8D` or `\u{20B9F}`. Moving out of the field or typing the Enter key converts all valid codes to standard Unicode code point format.

<img src="screenshots/pan-cjk-font-variants-horizontal.png" width="1080px" alt="Pan-CJK Font Variants (Horizontal) screenshot">

<img src="screenshots/pan-cjk-font-variants-vertical.png" width="1080px" alt="Pan-CJK Font Variants (Vertical) screenshot">

## Building

You'll need [Node.js](https://nodejs.org/) (which comes with [npm](https://www.npmjs.com/)) installed on your computer in order to build this application.

### Clone method

```bash
# Clone the repository
git clone https://codeberg.org/tonton-pixel/unicopedia-sinica
# Go into the repository
cd unicopedia-sinica
# Install dependencies
npm install
# Run the application
npm start
```

**Note**: to use the clone method, the core tool [git](https://www.git-scm.com/) must also be installed.

### Download method

If you don't wish to clone, you can [download the source code](https://codeberg.org/tonton-pixel/unicopedia-sinica/archive/master.zip), unZip it, then directly run the following commands from a Terminal opened at the resulting folder location:

```bash
# Install dependencies
npm install
# Run the application
npm start
```

### Packaging

Several scripts are also defined in the `package.json` file to build OS-specific bundles of the application, using the simple yet powerful [Electron Packager](https://github.com/electron-userland/electron-packager) Node module.\
For instance, running the following command (once the dependencies are installed) will create a `Unicopedia Sinica.app` version for macOS:

```bash
# Build macOS (Darwin) application
npm run build-darwin
```

## License

The MIT License (MIT).

Copyright © 2021-2025 Michel Mariani.
