//
let headStyle = document.createElement ('style');
document.head.appendChild (headStyle);
//
function easterEgg ()
{
    let message = "";
    if (headStyle.textContent)
    {
        headStyle.textContent = "";
    }
    else
    {
        const useMultipleFontFiles = true;
        let idsFontStack = getComputedStyle (document.documentElement).getPropertyValue ('--ids-family');
        let updatedIdsFontStack;
        if (useMultipleFontFiles)
        {
            updatedIdsFontStack = idsFontStack.replace ('"BabelStone Han"', '"BabelStone Han Basic", "BabelStone Han Extra"');
        }
        else
        {
            updatedIdsFontStack = idsFontStack.replace ('"BabelStone Han"', '"BabelStone Han Beta"');
        }
        if (updatedIdsFontStack !== idsFontStack)
        {
            headStyle.textContent = `:root { --ids-family: ${updatedIdsFontStack}; }`;
            if (useMultipleFontFiles)
            {
                message = "(BabelStone Han Basic/Extra)";
            }
            else
            {
                message = "(BabelStone Han Beta)";
            }
        }
    }
    return message;
}
//
module.exports = easterEgg;
//