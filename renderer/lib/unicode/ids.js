//
const operators =
{
    "⿰": { name: "IDC Left to Right", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿱": { name: "IDC Above to Below", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿲": { name: "IDC Left to Middle and Right", arity: 3 }, // \p{IDS_Trinary_Operator}
    "⿳": { name: "IDC Above to Middle and Below", arity: 3 }, // \p{IDS_Trinary_Operator}
    "⿴": { name: "IDC Full Surround", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿵": { name: "IDC Surround from Above", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿶": { name: "IDC Surround from Below", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿷": { name: "IDC Surround from Left", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿸": { name: "IDC Surround from Upper Left", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿹": { name: "IDC Surround from Upper Right", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿺": { name: "IDC Surround from Lower Left", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿻": { name: "IDC Overlaid", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿼": { name: "IDC Surround from Right", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿽": { name: "IDC Surround from Lower Right", arity: 2 }, // \p{IDS_Binary_Operator}
    "⿾": { name: "IDC Horizontal Reflection", arity: 1 }, // \p{IDS_Unary_Operator}
    "⿿": { name: "IDC Rotation", arity: 1 }, // \p{IDS_Unary_Operator}
    "㇯": { name: "IDC Subtraction", arity: 2 }, // \p{IDS_Binary_Operator}
    "〾": { name: "Ideographic Variation Indicator", arity: 1 }
};
//
const unihan = require ('../../lib/unicode/unihan.js');
//
function isValidOperand (token)
{
    let isValid =
    (
        (token === '？') // Fullwidth question mark, indicating an unrepresentable component
        ||
        (token === '！') // Fullwidth exclamation mark, indicating TBD decomposition
        ||
        (unihan.isUnified (token))   // CJK Unified Ideographs
        ||
        (/[\u2E80-\u2EF3]/.test (token))    // CJK Radicals Supplement
        ||
        (/[\u31C0-\u31E5]/.test (token))    // CJK Strokes
        ||
        (/[\uE000-\uF8FF]/.test (token))    // Private Use Area
    );
    return isValid;
}
//
function getTree (idsArray)
{
    let idsIndex = 0;
    function parseToken ()
    {
        let result = null;
        let token = idsArray[idsIndex++];
        if (token)
        {
            if (token in operators)
            {
                result = { };
                result.operator = token;
                result.operands = [ ];
                for (let index = 0; index < operators[token].arity; index++)
                {
                    result.operands[index] = parseToken ();
                }
            }
            else
            {
                result = token;
            }
        }
        return result;
    }
    return parseToken ();
};
//
function compare (idsArray)
{
    let idsIndex = 0;
    function parseToken ()
    {
        let result = null;
        let token = idsArray[idsIndex++];
        if (token)
        {
            if (token in operators)
            {
                result = { };
                result.operator = token;
                result.operands = [ ];
                for (let index = 0; index < operators[token].arity; index++)
                {
                    result.operands[index] = parseToken ();
                }
            }
            else
            {
                result = token;
            }
        }
        return result;
    }
    parseToken ();
    return idsArray.length - idsIndex;
};
//
module.exports = { operators, isValidOperand, getTree, compare }
//