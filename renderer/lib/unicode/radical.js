//
const unicode = require ('./unicode.js');
//
function getTooltip (character)
{
    let data = unicode.getCharacterData (character);
    let status = ((/[\u2E80-\u2EFF]/u.test (character)) ? "CJK": "KangXi") + " Radical";
    let lines =
    [
        `Code Point: ${data.codePoint}`,
        `Block: ${data.blockName}`,
        `Age: Unicode ${data.age} (${data.ageDate})`,
        `Status: ${status}`
    ];
    if (data.equivalentUnifiedIdeograph)
    {
        lines.push (`Equivalent CJK Unified Ideograph: ${data.equivalentUnifiedIdeograph}`);
    }
    return lines.join ("\n");
}
//
module.exports =
{
    getTooltip
};
//
