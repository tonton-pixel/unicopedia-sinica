//
// Temporary until Electron framework adds built-in support for Unicode 16.0
const rewritePattern = require ('regexpu-core');
//
// Support for Unicode 16.0 finally added in Electron 3x.x.x (xxxx-xx-xx) (Chromium xxx.0) (ICU xx.x)
// const rewritePattern = null;
//
function build (pattern, options)
{
    if (!options)
    {
        options = { };
    }
    let flags = 'u';
    if (!options.caseSensitive)
    {
         flags += 'i';
    }
    if (options.global)
    {
         flags += 'g';
    }
    if (!options.useRegex)
    {
         pattern = Array.from (pattern, (char) => `\\u{${char.codePointAt (0).toString (16)}}`).join ('');
    }
    if (options.wholeWord)
    {
        const beforeWordBoundary = '(?<![\\p{Alphabetic}\\p{Mark}\\p{Decimal_Number}\\p{Connector_Punctuation}\\p{Join_Control}])';
        const afterWordBoundary = '(?![\\p{Alphabetic}\\p{Mark}\\p{Decimal_Number}\\p{Connector_Punctuation}\\p{Join_Control}])';
        pattern = `${beforeWordBoundary}(?:${pattern})${afterWordBoundary}`;
    }
    if (rewritePattern)
    {
        pattern = rewritePattern (pattern, flags, { unicodePropertyEscapes: 'transform' });
    }
    return new RegExp (pattern, flags);
};
//
module.exports =
{
    build
};
//
