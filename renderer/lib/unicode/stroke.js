//
const unicode = require ('./unicode.js');
//
const strokeRegex = /^[\u31C0-\u31E5]$/; // [㇀-㇥]
//
function isStroke (character)
{
    return strokeRegex.test (character);
}
//
function getTooltip (character)
{
    let data = unicode.getCharacterData (character);
    let name = data.name.replace ("STROKE", "Stroke");
    let lines =
    [
        `Code Point: ${data.codePoint}`,
        `Block: ${data.blockName}`,
        `Age: Unicode ${data.age} (${data.ageDate})`,
        `Name: ${name}`
    ];
    return lines.join ("\n");
}
//
module.exports =
{
    isStroke,
    getTooltip
};
//
