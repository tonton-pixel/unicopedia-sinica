//
const unitId = 'cjk-local-fonts-unit';
//
const unit = document.getElementById (unitId);
//
const historyButton = unit.querySelector ('.history-button');
const unihanInput = unit.querySelector ('.unihan-input');
const lookUpButton = unit.querySelector ('.look-up-button');
const compactLayoutCheckbox = unit.querySelector ('.compact-layout-checkbox');
const characterReference = unit.querySelector ('.character-reference');
const useEastAsianCheckbox = unit.querySelector ('.use-east-asian');
const eastAsianSelect = unit.querySelector ('.east-asian-select');
const nameFilterInput = unit.querySelector ('.name-filter-input');
const filteredCountNumber = unit.querySelector ('.filtered-count-number');
const totalCountNumber = unit.querySelector ('.total-count-number');
const errorMessage = unit.querySelector ('.error-message');
const progressMessage = unit.querySelector ('.progress-message');
const fontGlyphsContainer = unit.querySelector ('.font-glyphs-container');
const instructions = unit.querySelector ('.instructions');
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
const unihanHistorySize = 128;   // 0: unlimited
//
let unihanHistory = [ ];
let unihanHistoryIndex = -1;
let unihanHistorySave = null;
//
let currentUnihanSequence;
//
module.exports.start = function (context)
{
    const { shell } = require ('electron');
    const { Menu } = require ('@electron/remote');
    //
    const pullDownMenus = require ('../../lib/pull-down-menus.js');
    const linksList = require ('../../lib/links-list.js');
    //
    const unicode = require ('../../lib/unicode/unicode.js');
    const unihan = require ('../../lib/unicode/unihan.js');
    const radical = require ('../../lib/unicode/radical.js');
    const stroke = require ('../../lib/unicode/stroke.js');
    //
    const refLinks = require ('./ref-links.json');
    //
    const fontList = require ('font-list');
    //
    let localFonts = null;
    //
    const defaultPrefs =
    {
        unihanHistory: [ ],
        unihanSequence: "",
        useEastAsianCheckbox: false,
        eastAsianSelect: "",
        nameFilterInput: "",
        compactLayout: false,
        instructions: true,
        references: false,
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    let headStyle = document.createElement ('style');
    document.head.appendChild (headStyle);
    //
    unihanHistory = prefs.unihanHistory;
    //
    const defaultFontSize = 48;
    const defaultCanvasSize = 60;
    //
    let canvas = document.createElement ('canvas');
    canvas.width = defaultCanvasSize;
    canvas.height = defaultCanvasSize;
    let ctx = canvas.getContext ('2d', { alpha: false, desynchronized: true });
    //
    function getTextData (text)
    {
        ctx.fillStyle = "white";
        ctx.fillRect (0, 0, ctx.canvas.width, ctx.canvas.height);
        if (text)
        {
            let textMetrics = ctx.measureText (text);
            let actualWidth = textMetrics.actualBoundingBoxRight - textMetrics.actualBoundingBoxLeft;
            let actualHeight = textMetrics.actualBoundingBoxAscent - textMetrics.actualBoundingBoxDescent;
            ctx.fillStyle = "black";
            ctx.fillText (text, (ctx.canvas.width - actualWidth) / 2, ctx.canvas.height - ((ctx.canvas.height - actualHeight) / 2));
        }
        return ctx.getImageData (0, 0, ctx.canvas.width, ctx.canvas.height).data.toString ();
    }
    //
    let currentDiffElement = null;
    //
    function showBase (event)
    {
        if (!(event.button || event.ctrlKey || event.metaKey))
        {
            if (event.altKey || event.shiftKey)
            {
                event.preventDefault ();
                currentDiffElement = event.currentTarget;
                let glyph = currentDiffElement.querySelector ('.glyph');
                let [ base ] = currentUnihanSequence;
                glyph.textContent = base;
                currentDiffElement.classList.remove ('vs-difference');
                document.addEventListener ('mouseup', hideBase, { once: true });
            }
        }
    }
    //
    function hideBase (event)
    {
        if (currentDiffElement)
        {
            event.preventDefault ();
            let glyph = currentDiffElement.querySelector ('.glyph');
            glyph.textContent = currentUnihanSequence;
            currentDiffElement.classList.add ('vs-difference');
            currentDiffElement = null;
        }
    }
    //
    async function displayLookUpData (unihanSequence)
    {
        while (fontGlyphsContainer.firstChild)
        {
            fontGlyphsContainer.firstChild.remove ();
        }
        currentUnihanSequence = unihanSequence;
        if (unihanSequence)
        {
            let indexOfUnihanSequence = unihanHistory.indexOf (unihanSequence);
            if (indexOfUnihanSequence !== -1)
            {
                unihanHistory.splice (indexOfUnihanSequence, 1);
            }
            unihanHistory.unshift (unihanSequence);
            if ((unihanHistorySize > 0) && (unihanHistory.length > unihanHistorySize))
            {
                unihanHistory.pop ();
            }
            unihanHistoryIndex = -1;
            unihanHistorySave = null;
            //
            let list = document.createElement ('div');
            list.className = 'glyph-list';
            if (!localFonts)
            {
                try
                {
                    progressMessage.classList.remove ('hidden');
                    localFonts = await fontList.getFonts ({ disableQuoting: true });
                    progressMessage.classList.add ('hidden');
                }
                catch (e)
                {
                    console.log (e);
                }
            }
            if (localFonts)
            {
                for (let localFont of localFonts)
                {
                    ctx.font = `${defaultFontSize}px "${localFont}", "Blank"`;
                    let textMetrics = ctx.measureText (unihanSequence);
                    let actualWidth = textMetrics.actualBoundingBoxRight - textMetrics.actualBoundingBoxLeft;
                    if (actualWidth > 0)
                    {
                        ctx.font = `${defaultFontSize}px "${localFont}"`;
                        let card =  document.createElement ('span');
                        card.className = 'card';
                        let [ base, vs ] = unihanSequence;
                        if (vs)
                        {
                            if (getTextData (unihanSequence) !== getTextData (base))
                            {
                                card.classList.add ('vs-difference');
                                card.addEventListener ('mousedown', showBase);
                            }
                        }
                        let glyph = document.createElement ('span');
                        glyph.className = 'glyph';
                        glyph.textContent = unihanSequence;
                        glyph.style = `font-family: ${localFont};`;
                        glyph.title = `Local font: ${localFont}`;
                        card.appendChild (glyph);
                        let fontName = document.createElement ('span');
                        fontName.className = 'font-name';
                        fontName.textContent = localFont;
                        fontName.title = `Local font: ${localFont}`;
                        card.appendChild (fontName);
                        list.appendChild (card);
                    }
                }
                fontGlyphsContainer.appendChild (list);
                if (compactLayoutCheckbox.checked)
                {
                    fontGlyphsContainer.classList.add ('compact');
                }
            }
        }
        doFilter (nameFilterInput.value);
    }
    //
    const ivdRegex = /^<(?:[Uu]\+?)?([0-9a-fA-F]{4,8})(?:,\s*|\s+)(?:[Uu]\+?)?([0-9a-fA-F]{4,8})>$/u;
    const glyphWikiRegex = /^(?:[Uu]\+?)?([0-9a-fA-F]{4,8})-(?:[Uu]\+?)?([0-9a-fA-F]{4,8})$/u;
    const codePointsRegex = /^(?:[Uu]\+?)?([0-9a-fA-F]{4,8})(\s+(?:[Uu]\+?)?([0-9a-fA-F]{4,8}))?$/u;
    //
    function validateUnihanSequenceInput (inputString)
    {
        let sequence = "";
        inputString = inputString.trim ();
        if (unihan.isUnihanVariation (inputString) || unihan.isUnihan (inputString) || unihan.isRadical (inputString) || stroke.isStroke (inputString))
        {
            sequence = inputString;
        }
        else
        {
            let match = inputString.match (ivdRegex) || inputString.match (glyphWikiRegex) || inputString.match (codePointsRegex);
            if (match)
            {
                inputString = unicode.codePointsToCharacters (inputString);
                if (unihan.isUnihanVariation (inputString) || unihan.isUnihan (inputString) || unihan.isRadical (inputString) || stroke.isStroke (inputString))
                {
                    sequence = inputString;
                }
            }
        }
        return sequence;
    }
    //
    unihanInput.addEventListener
    (
        'input',
        (event) =>
        {
            event.currentTarget.classList.remove ('invalid');
            if (event.currentTarget.value)
            {
                if (!validateUnihanSequenceInput (event.currentTarget.value))
                {
                    event.currentTarget.classList.add ('invalid');
                }
            }
        }
    );
    unihanInput.addEventListener
    (
        'keypress',
        (event) =>
        {
            if (event.key === 'Enter')
            {
                event.preventDefault ();
                lookUpButton.click ();
            }
        }
    );
    unihanInput.addEventListener
    (
        'keydown',
        (event) =>
        {
            if (event.altKey)
            {
                if (event.key === 'ArrowUp')
                {
                    event.preventDefault ();
                    if (unihanHistoryIndex === -1)
                    {
                        unihanHistorySave = event.currentTarget.value;
                    }
                    unihanHistoryIndex++;
                    if (unihanHistoryIndex > (unihanHistory.length - 1))
                    {
                        unihanHistoryIndex = (unihanHistory.length - 1);
                    }
                    if (unihanHistoryIndex !== -1)
                    {
                        event.currentTarget.value = unihanHistory[unihanHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
                else if (event.key === 'ArrowDown')
                {
                    event.preventDefault ();
                    unihanHistoryIndex--;
                    if (unihanHistoryIndex < -1)
                    {
                        unihanHistoryIndex = -1;
                        unihanHistorySave = null;
                    }
                    if (unihanHistoryIndex === -1)
                    {
                        if (unihanHistorySave !== null)
                        {
                            event.currentTarget.value = unihanHistorySave;
                            event.currentTarget.dispatchEvent (new Event ('input'));
                        }
                    }
                    else
                    {
                        event.currentTarget.value = unihanHistory[unihanHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
            }
        }
    );
    //
    function vsToVSIndex (vs) 
    {
        let vsCode = vs.codePointAt (0);
        return (vsCode >= 0xE0100 ? vsCode - 0xE0100 + 16: vsCode - 0xFE00) + 1;
    }
    //
    function vsIndexToVS (vsIndex) 
    {
        let vs = ""
        if ((vsIndex >= 1) && (vsIndex <= 16))
        {
            vs = String.fromCodePoint (0xFE00 + vsIndex - 1);
        }
        else if ((vsIndex >= 17) && (vsIndex <= 256))
        {
            vs = String.fromCodePoint (0xE0100 + vsIndex - 17);
        }
        return vs;
    }
    //
    function updateUnihanData (sequence)
    {
        while (characterReference.firstChild)
        {
            characterReference.firstChild.remove ();
        }
        if (sequence)
        {
            let characterInfo = document.createElement ('span');
            characterInfo.className = 'character-info';
            let [ base, vs ] = sequence;
            let baseCharacter = document.createElement ('span');
            baseCharacter.className = 'base-character';
            let characterGlyph = document.createElement ('span');
            characterGlyph.className = 'character-glyph';
            characterGlyph.textContent = base;
            baseCharacter.appendChild (characterGlyph);
            let characterCodePoint = document.createElement ('span');
            characterCodePoint.className = 'character-code-point';
            if (unihan.isCompatibility (base))
            {
                characterCodePoint.classList.add ('compatibility');
            }
            characterCodePoint.textContent = unicode.characterToCodePoint (base);
            if (unihan.isRadical (base))
            {
                characterGlyph.classList.add ('radical');
                baseCharacter.title = radical.getTooltip (base);
            }
            else if (stroke.isStroke (base))
            {
                characterGlyph.classList.add ('stroke');
                baseCharacter.title = stroke.getTooltip (base)
            }
            else
            {
                baseCharacter.title = unihan.getExpandedTooltip (base);
            }
            baseCharacter.appendChild (characterCodePoint);
            characterInfo.appendChild (baseCharacter);
            if (unihan.isRadical (base))
            {
                let radicalInfo = document.createElement ('span');
                radicalInfo.className = 'radical-info';
                radicalInfo.textContent = "(Radical)";
                characterInfo.appendChild (radicalInfo);
            }
            else if (stroke.isStroke (base))
            {
                let strokeInfo = document.createElement ('span');
                strokeInfo.className = 'stroke-info';
                strokeInfo.textContent = "(Stroke)";
                characterInfo.appendChild (strokeInfo);
            }
            if (vs)
            {
                let plusJoiner = document.createElement ('span');
                plusJoiner.className = 'plus-joiner';
                plusJoiner.textContent = "+";
                characterInfo.appendChild (plusJoiner);
                let vsCharacter = document.createElement ('span');
                vsCharacter.className = 'vs-character';
                let characterGlyph = document.createElement ('span');
                characterGlyph.className = 'character-glyph';
                let vsIndex = vsToVSIndex (vs);
                characterGlyph.appendChild (document.createTextNode ("VS"));
                characterGlyph.appendChild (document.createElement ('br'));
                characterGlyph.appendChild (document.createTextNode (vsIndex));
                vsCharacter.appendChild (characterGlyph);
                let characterCodePoint = document.createElement ('span');
                characterCodePoint.className = 'character-code-point';
                characterCodePoint.textContent = unicode.characterToCodePoint (vs);
                vsCharacter.appendChild (characterCodePoint);
                vsCharacter.title = `VARIATION SELECTOR-${vsIndex} (VS${vsIndex})`;
                characterInfo.appendChild (vsCharacter);
            }
            characterReference.appendChild (characterInfo);
            let allowVS = unihan.isUnified (base);
            let vsSelector = document.createElement ('span');
            vsSelector.className = 'vs-selector';
            let vsSelectLabel = document.createElement ('label');
            vsSelectLabel.className = 'vs-label';
            if (!allowVS)
            {
                vsSelectLabel.classList.add ('disabled');
            }
            vsSelectLabel.appendChild (document.createTextNode ("VS:\xA0"));
            let vsSelect = document.createElement ('select');
            vsSelect.className = 'vs-select';
            vsSelect.disabled = !allowVS;
            for (let vsIndex = 1; vsIndex <= 16; vsIndex++)
            {
                let vsOption = document.createElement ('option');
                vsOption.value = vsIndex;
                vsOption.textContent = vsIndex;
                vsOption.title = `<${unicode.characterToCodePoint (vsIndexToVS (vsIndex))}> VARIATION SELECTOR-${vsIndex}`;
                vsSelect.appendChild (vsOption);
            }
            let noneOption = document.createElement ('option');
            noneOption.value = 0;
            noneOption.textContent = "--";
            noneOption.title = "No Variation Selector";
            vsSelect.appendChild (noneOption);
            for (let vsIndex = 17; vsIndex <= 256; vsIndex++)
            {
                let vsOption = document.createElement ('option');
                vsOption.value = vsIndex;
                vsOption.textContent = vsIndex;
                vsOption.title = `<${unicode.characterToCodePoint (vsIndexToVS (vsIndex))}> VARIATION SELECTOR-${vsIndex}`;
                vsSelect.appendChild (vsOption);
            }
            if (vs)
            {
                vsSelect.value = vsToVSIndex (vs);
            }
            else
            {
                vsSelect.value = 0;
            }
            if (allowVS)
            {
                vsSelect.addEventListener
                (
                    'input',
                    event =>
                    {
                        let vsIndex = parseInt (event.currentTarget.value);
                        let vs = vsIndexToVS (vsIndex);
                        updateUnihanData (base + vs);
                    }
                );
            }
            vsSelectLabel.appendChild (vsSelect);
            vsSelector.appendChild (vsSelectLabel);
            characterReference.appendChild (vsSelector);
        }
        unihanInput.value = "";
        unihanInput.blur ();
        unihanInput.dispatchEvent (new Event ('input'));
        displayLookUpData (sequence);
        unit.scrollTop = 0;
        unit.scrollLeft = 0;
    }
    //
    lookUpButton.addEventListener
    (
        'click',
        (event) =>
        {
            if (unihanInput.value)
            {
                let sequence = validateUnihanSequenceInput (unihanInput.value);
                if (sequence)
                {
                    updateUnihanData (sequence);
                }
                else
                {
                    shell.beep ();
                }
            }
            else
            {
                unihanHistoryIndex = -1;
                unihanHistorySave = null;
                updateUnihanData ("");
            }
        }
    );
    //
    function insertUnihanSequence (menuItem)
    {
        unihanInput.value = menuItem.id;
        unihanInput.dispatchEvent (new Event ('input'));
        lookUpButton.click ();
    };
    historyButton.addEventListener
    (
        'click',
        (event) =>
        {
            let historyMenuTemplate = [ ];
            historyMenuTemplate.push ({ label: "Lookup History", enabled: false })
            // historyMenuTemplate.push ({ type: 'separator' })
            if (unihanHistory.length > 0)
            {
                for (let unihan of unihanHistory)
                {
                    let [ base, vs ] = unihan;
                    let label = `${base}${(process.platform === 'darwin') ? "\t" : "\xA0\xA0"}${unicode.characterToCodePoint (base)}`;
                    if (vs)
                    {
                        label += `\xA0\xA0${unicode.characterToCodePoint (vs)}`;
                    }
                    historyMenuTemplate.push
                    (
                        {
                            label: label,
                            id: unihan,
                            toolTip: Array.from (unihan).map (char => unicode.getCharacterBasicData (char).name).join (",\n"),
                            click: insertUnihanSequence
                        }
                    );
                }
            }
            else
            {
                historyMenuTemplate.push ({ label: "(no history yet)", enabled: false });
            }
            let historyContextualMenu = Menu.buildFromTemplate (historyMenuTemplate);
            pullDownMenus.popup (event.currentTarget, historyContextualMenu, 0);
        }
    );
    //
    compactLayoutCheckbox.checked = prefs.compactLayout;
    compactLayoutCheckbox.addEventListener
    (
        'input',
        (event) =>
        {
            if (event.currentTarget.checked)
            {
                fontGlyphsContainer.classList.add ('compact');
            }
            else
            {
                fontGlyphsContainer.classList.remove ('compact');
            }
        }
    );
    //
    function doFilter (string)
    {
        let filteredCount = 0;
        let totalCount = 0;
        let cards = unit.querySelectorAll ('.glyph-list .card');
        if (cards)
        {
            for (let card of cards)
            {
                card.classList.remove ('is-shown');
                let fontName = card.querySelector ('.font-name').textContent;
                if ((!string) || (fontName.toUpperCase ().indexOf (string.toUpperCase ()) > -1))
                {
                    card.classList.add ('is-shown');
                    filteredCount++;
                }
                totalCount++;
            }
        }
        filteredCountNumber.textContent = filteredCount;
        totalCountNumber.textContent = totalCount;
        fontGlyphsContainer.hidden = (filteredCount === 0);
        errorMessage.hidden = true;
        errorMessage.textContent = "";
        if (currentUnihanSequence)
        {
            errorMessage.hidden = false;
            if (totalCount === 0)
            {
                errorMessage.textContent = "No suitable font available";
            }
            else if (filteredCount === 0)
            {
                errorMessage.textContent = "No matching font name";
            }
        }
    }
    //
    useEastAsianCheckbox.checked = prefs.useEastAsianCheckbox;
    useEastAsianCheckbox.addEventListener
    (
        'input',
        event =>
        {
            eastAsianSelect.disabled = !event.currentTarget.checked;
            eastAsianSelect.dispatchEvent (new Event ('input'));
        }
    );
    //
    eastAsianSelect.value = prefs.eastAsianSelect;
    if (eastAsianSelect.selectedIndex < 0) // -1: no element is selected
    {
        eastAsianSelect.selectedIndex = 0;
    }
    eastAsianSelect.disabled = !useEastAsianCheckbox.checked;
    eastAsianSelect.addEventListener
    (
        'input',
        event =>
        {
            if (!useEastAsianCheckbox.checked)
            {
                headStyle.textContent = ``;
            }
            else
            {
                headStyle.textContent = `#${unitId} .font-glyphs-container .glyph { font-variant-east-asian: ${event.currentTarget.value}; }`;
            }
        }
    );
    eastAsianSelect.dispatchEvent (new Event ('input'));
    //
    nameFilterInput.value = prefs.nameFilterInput;
    nameFilterInput.addEventListener ('input', (event) => { doFilter (event.currentTarget.value); });
    //
    currentUnihanSequence = prefs.unihanSequence;
    updateUnihanData (currentUnihanSequence);
    //
    instructions.open = prefs.instructions;
    //
    references.open = prefs.references;
    //
    linksList (links, refLinks);
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        unihanHistory: unihanHistory,
        unihanSequence: currentUnihanSequence,
        useEastAsianCheckbox: useEastAsianCheckbox.checked,
        eastAsianSelect: eastAsianSelect.value,
        nameFilterInput: nameFilterInput.value,
        compactLayout: compactLayoutCheckbox.checked,
        instructions: instructions.open,
        references: references.open
    };
    context.setPrefs (prefs);
};
//
