//
const unitId = 'cjk-radicals-unit';
//
const unit = document.getElementById (unitId);
//
const liveSearch = unit.querySelector ('.live-search');
const wholeWordCheckbox = unit.querySelector ('.whole-word-checkbox');
const sheet = unit.querySelector ('.sheet');
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
const instructions = unit.querySelector ('.instructions');
//
let defaultFolderPath;
//
module.exports.start = function (context)
{
    const linksList = require ('../../lib/links-list.js');
    //
    const regexp = require ('../../lib/unicode/regexp.js');
    const unicode = require ('../../lib/unicode/unicode.js');
    //
    const kangxiRadicals = require ('../../lib/unicode/kangxi-radicals.json');
    //
    const defaultPrefs =
    {
        liveSearch: "",
        wholeWord: false,
        instructions: true,
        references: false,
        defaultFolderPath: context.defaultFolderPath
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    function matchString (searchRegex, kangXiRadical)
    {
        let match = false;
        let fields =
        [
            kangXiRadical.number,
            "KangXi Radical " + kangXiRadical.name,
            kangXiRadical.radical,
            kangXiRadical.unified,
            kangXiRadical.radical.normalize ('NFKC'),
            kangXiRadical.radical.normalize ('NFKD'),
            kangXiRadical.substitute || kangXiRadical.alternate
        ];
        for (let field of fields)
        {
            if (searchRegex.test (field))
            {
                match = true;
                break;
            }
        }
        if ((!match) && kangXiRadical.cjk)
        {
            for (let cjkRadical of kangXiRadical.cjk)
            {
                let fields =
                [
                    cjkRadical.number,
                    "CJK Radical " + cjkRadical.name,
                    cjkRadical.radical,
                    cjkRadical.unified,
                    cjkRadical.radical.normalize ('NFKC'),
                    cjkRadical.radical.normalize ('NFKD'),
                    cjkRadical.substitute || cjkRadical.alternate
                ];
                for (let field of fields)
                {
                    if (searchRegex.test (field))
                    {
                        match = true;
                        break;
                    }
                }
            }
        }
        return match;
    }
    //
    function getNameTooltip (character)
    {
        let data = unicode.getCharacterBasicData (character);
        return `<${data.codePoint}>\xA0${data.name}`;
    }
    //
    function createTable (searchString, wholeWord)
    {
        let table = document.createElement ('table');
        table.className = 'table';
        let row = document.createElement ('tr');
        let number = document.createElement ('th');
        number.className = 'number';
        number.textContent = "Number";
        number.title = "Radical number (1 to 214)";
        row.appendChild (number);
        let fullName = document.createElement ('th');
        fullName.className = 'full-name';
        fullName.textContent = "Full Name";
        fullName.title = "Radical full name";
        row.appendChild (fullName);
        let codePoint = document.createElement ('th');
        codePoint.textContent = "Code Point";
        codePoint.className = 'radical-code-point';
        codePoint.title = "Radical code point";
        row.appendChild (codePoint);
        let radical = document.createElement ('th');
        radical.className = 'radical';
        radical.textContent = "Radical";
        radical.title = "Code chart representative glyph";
        row.appendChild (radical);
        let localFont = document.createElement ('th');
        localFont.className = 'local-font';
        localFont.textContent = "Local Font";
        localFont.title = "Default local glyph rendering";
        row.appendChild (localFont);
        let equivalentUnified = document.createElement ('th');
        equivalentUnified.className = 'equivalent-unified';
        equivalentUnified.textContent = "Eq. Unified";
        equivalentUnified.title = "Equivalent CJK unified ideograph";
        row.appendChild (equivalentUnified);
        let normalized = document.createElement ('th');
        normalized.className = 'normalized';
        normalized.textContent = "Normalized";
        normalized.title = "Normalized radical (NFKC/NFKD)";
        row.appendChild (normalized);
        let other = document.createElement ('th');
        other.className = 'other';
        other.textContent = "Other";
        other.title = "Substitute/Alternate";
        row.appendChild (other);
        table.appendChild (row);
        let matchCount = 0;
        let characters = unicode.codePointsToCharacters (searchString);
        if (Array.from (characters).length === 1)
        {
            searchString = characters;
        }
        let searchRegex = regexp.build (searchString, { wholeWord: wholeWord });
        for (let kangXiRadical of kangxiRadicals)
        {
            if ((!searchString) || matchString (searchRegex, kangXiRadical))
            {
                matchCount++;
                let row = document.createElement ('tr');
                let number = document.createElement ('td');
                if (kangXiRadical.cjk)
                {
                    number.setAttribute ('rowspan', 1 + kangXiRadical.cjk.length)
                }
                number.className = 'number';
                let kangXiNumber = document.createElement ('span');
                kangXiNumber.className = 'kangxi-number';
                kangXiNumber.textContent = kangXiRadical.number;
                number.appendChild (kangXiNumber);
                row.appendChild (number);
                let fullName = document.createElement ('td');
                fullName.className = 'full-name';
                fullName.textContent = "KangXi\xA0Radical " + kangXiRadical.name;
                row.appendChild (fullName);
                let codePoint = document.createElement ('td');
                codePoint.className = 'radical-code-point';
                codePoint.textContent = unicode.characterToCodePoint (kangXiRadical.radical);
                row.appendChild (codePoint);
                let radical = document.createElement ('td');
                radical.className = 'radical';
                let radicalGlyph = document.createElement ('span');
                radicalGlyph.className = 'glyph';
                radicalGlyph.textContent = kangXiRadical.radical;
                radical.appendChild (radicalGlyph);
                let radicalCodePoint = document.createElement ('span');
                radicalCodePoint.className = 'code-point';
                radicalCodePoint.textContent = unicode.characterToCodePoint (kangXiRadical.radical);
                radical.appendChild (radicalCodePoint);
                radical.title = getNameTooltip (kangXiRadical.radical);
                row.appendChild (radical);
                let localFont = document.createElement ('td');
                localFont.className = 'local-font';
                let localGlyph = document.createElement ('span');
                localGlyph.className = 'glyph';
                localGlyph.textContent = kangXiRadical.radical;
                localFont.appendChild (localGlyph);
                let localCodePoint = document.createElement ('span');
                localCodePoint.className = 'code-point';
                localCodePoint.textContent = unicode.characterToCodePoint (kangXiRadical.radical);
                localFont.appendChild (localCodePoint);
                localFont.title = getNameTooltip (kangXiRadical.radical);
                row.appendChild (localFont);
                let equivalentUnified = document.createElement ('td');
                equivalentUnified.className = 'equivalent-unified';
                let unifiedGlyph = document.createElement ('span');
                unifiedGlyph.className = 'glyph';
                unifiedGlyph.textContent = kangXiRadical.unified;
                equivalentUnified.appendChild (unifiedGlyph);
                let unifiedCodePoint = document.createElement ('span');
                unifiedCodePoint.className = 'code-point';
                unifiedCodePoint.textContent = unicode.characterToCodePoint (kangXiRadical.unified);
                equivalentUnified.appendChild (unifiedCodePoint);
                equivalentUnified.title = getNameTooltip (kangXiRadical.unified);
                row.appendChild (equivalentUnified);
                let normalized = document.createElement ('td');
                normalized.className = 'normalized';
                let normalizedKC = kangXiRadical.radical.normalize ('NFKC');
                let normalizedKD = kangXiRadical.radical.normalize ('NFKD');
                if (normalizedKC !== normalizedKD) console.log ("NFKC/NFKD differ for", kangXiRadical.radical);
                if ((normalizedKC !== kangXiRadical.radical) && (normalizedKD !== kangXiRadical.radical))
                {
                    let normalizedGlyph = document.createElement ('span');
                    normalizedGlyph.className = 'glyph';
                    normalizedGlyph.textContent = normalizedKC;
                    normalized.appendChild (normalizedGlyph);
                    let normalizedCodePoint = document.createElement ('span');
                    normalizedCodePoint.className = 'code-point';
                    normalizedCodePoint.textContent = unicode.characterToCodePoint (normalizedKD);
                    normalized.appendChild (normalizedCodePoint);
                    normalized.title = getNameTooltip (normalizedKD);
                }
                else
                {
                    normalized.classList.add ('empty');
                }
                row.appendChild (normalized);
                let other = document.createElement ('td');
                other.className = 'other';
                let otherCharacter = kangXiRadical.substitute || kangXiRadical.alternate;
                if (otherCharacter)
                {
                    let otherGlyph = document.createElement ('span');
                    otherGlyph.className = 'glyph';
                    otherGlyph.textContent = otherCharacter;
                    other.appendChild (otherGlyph);
                    let otherCodePoint = document.createElement ('span');
                    otherCodePoint.className = 'code-point';
                    otherCodePoint.textContent = unicode.characterToCodePoint (otherCharacter);
                    other.appendChild (otherCodePoint);
                    other.title = getNameTooltip (otherCharacter);
                }
                else
                {
                    other.classList.add ('empty');
                }
                row.appendChild (other);
                table.appendChild (row);
                if (kangXiRadical.cjk)
                {
                    for (let cjkRadical of kangXiRadical.cjk)
                    {
                        let row = document.createElement ('tr');
                        let fullName = document.createElement ('td');
                        fullName.className = 'full-name';
                        if (!cjkRadical.radical)
                        {
                            fullName.classList.add ('no-radical');
                        }
                        fullName.textContent = "CJK\xA0Radical " + cjkRadical.name;
                        fullName.innerHTML = fullName.innerHTML.replace (/.-Simplified/g, "<span style=\"white-space: nowrap;\">$&</span>");
                        row.appendChild (fullName);
                        let codePoint = document.createElement ('td');
                        codePoint.className = 'radical-code-point';
                        if (cjkRadical.radical)
                        {
                            codePoint.textContent = unicode.characterToCodePoint (cjkRadical.radical);
                        }
                        else
                        {
                            codePoint.classList.add ('empty');
                        }
                        row.appendChild (codePoint);
                        let radical = document.createElement ('td');
                        radical.className = 'radical';
                        if (cjkRadical.radical)
                        {
                            let radicalGlyph = document.createElement ('span');
                            radicalGlyph.className = 'glyph';
                            radicalGlyph.textContent = cjkRadical.radical;
                            radical.appendChild (radicalGlyph);
                            let radicalCodePoint = document.createElement ('span');
                            radicalCodePoint.className = 'code-point';
                            radicalCodePoint.textContent = unicode.characterToCodePoint (cjkRadical.radical);
                            radical.appendChild (radicalCodePoint);
                            radical.title = getNameTooltip (cjkRadical.radical);
                        }
                        else
                        {
                            radical.classList.add ('empty');
                        }
                        row.appendChild (radical);
                        let localFont = document.createElement ('td');
                        localFont.className = 'local-font';
                        if (cjkRadical.radical)
                        {
                            let localGlyph = document.createElement ('span');
                            localGlyph.className = 'glyph';
                            localGlyph.textContent = cjkRadical.radical;
                            localFont.appendChild (localGlyph);
                            let localCodePoint = document.createElement ('span');
                            localCodePoint.className = 'code-point';
                            localCodePoint.textContent = unicode.characterToCodePoint (cjkRadical.radical);
                            localFont.appendChild (localCodePoint);
                            localFont.title = getNameTooltip (cjkRadical.radical);
                        }
                        else if (cjkRadical.unified)
                        {
                            localFont.classList.add ('empty');
                        }
                        row.appendChild (localFont);
                        let equivalentUnified = document.createElement ('td');
                        equivalentUnified.className = 'equivalent-unified';
                        if (!cjkRadical.radical)
                        {
                            equivalentUnified.classList.add ('no-radical');
                        }
                        let unifiedGlyph = document.createElement ('span');
                        unifiedGlyph.className = 'glyph';
                        unifiedGlyph.textContent = cjkRadical.unified;
                        equivalentUnified.appendChild (unifiedGlyph);
                        let unifiedCodePoint = document.createElement ('span');
                        unifiedCodePoint.className = 'code-point';
                        unifiedCodePoint.textContent = unicode.characterToCodePoint (cjkRadical.unified);
                        equivalentUnified.appendChild (unifiedCodePoint);
                        equivalentUnified.title = getNameTooltip (cjkRadical.unified);
                        row.appendChild (equivalentUnified);
                        let normalized = document.createElement ('td');
                        normalized.className = 'normalized';
                        let normalizedKC = cjkRadical.radical.normalize ('NFKC');
                        let normalizedKD = cjkRadical.radical.normalize ('NFKD');
                        if (normalizedKC !== normalizedKD) console.log ("NFKC/NFKD differ for", cjkRadical.radical);
                        if ((normalizedKC !== cjkRadical.radical) && (normalizedKD !== cjkRadical.radical))
                        {
                            let normalizedGlyph = document.createElement ('span');
                            normalizedGlyph.className = 'glyph';
                            normalizedGlyph.textContent = normalizedKC;
                            normalized.appendChild (normalizedGlyph);
                            let normalizedCodePoint = document.createElement ('span');
                            normalizedCodePoint.className = 'code-point';
                            normalizedCodePoint.textContent = unicode.characterToCodePoint (normalizedKD);
                            normalized.appendChild (normalizedCodePoint);
                            normalized.title = getNameTooltip (normalizedKD);
                        }
                        else
                        {
                            normalized.classList.add ('empty');
                        }
                        row.appendChild (normalized);
                        let other = document.createElement ('td');
                        other.className = 'other';
                        let otherCharacter = cjkRadical.substitute || cjkRadical.alternate;
                        if (otherCharacter)
                        {
                            let otherGlyph = document.createElement ('span');
                            otherGlyph.className = 'glyph';
                            otherGlyph.textContent = otherCharacter;
                            other.appendChild (otherGlyph);
                            let otherCodePoint = document.createElement ('span');
                            otherCodePoint.className = 'code-point';
                            otherCodePoint.textContent = unicode.characterToCodePoint (otherCharacter);
                            other.appendChild (otherCodePoint);
                            other.title = getNameTooltip (otherCharacter);
                        }
                        else
                        {
                            other.classList.add ('empty');
                        }
                        row.appendChild (other);
                        table.appendChild (row);
                    }
                }
            }
        }
        if (matchCount === 0)
        {
            let row = document.createElement ('tr');
            let message = document.createElement ('td');
            message.className = 'message';
            message.setAttribute ('colspan', 8); // !!
            message.textContent = "No Match";
            row.appendChild (message);
            table.appendChild (row);
        }
        table.appendChild (row.cloneNode (true));
        return table;
    }
    //
    function doSearch (string, wholeWord)
    {
        while (sheet.firstChild)
        {
            sheet.firstChild.remove ();
        }
        sheet.appendChild (createTable (string, wholeWord));
    }
    //
    wholeWordCheckbox.checked = prefs.wholeWord;
    wholeWordCheckbox.addEventListener
    (
        'input',
        (event) => doSearch (liveSearch.value, event.currentTarget.checked)
    );
    //
    liveSearch.value = prefs.liveSearch;
    liveSearch.addEventListener ('input', (event) => { doSearch (event.currentTarget.value, wholeWordCheckbox.checked); });
    //
    doSearch (liveSearch.value, wholeWordCheckbox.checked);
    //
    const refLinks = require ('./ref-links.json');
    //
    instructions.open = prefs.instructions;
    //
    references.open = prefs.references;
    //
    linksList (links, refLinks);
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        liveSearch: liveSearch.value,
        wholeWord: wholeWordCheckbox.checked,
        instructions: instructions.open,
        references: references.open,
        defaultFolderPath: defaultFolderPath
    };
    context.setPrefs (prefs);
};
//
