//
const unit = document.getElementById ('cjk-related-unit');
//
const historyButton = unit.querySelector ('.history-button');
const unihanInput = unit.querySelector ('.unihan-input');
const lookUpButton = unit.querySelector ('.look-up-button');
const extrapolatedCheckbox = unit.querySelector ('.extrapolated-checkbox');
const linearCharacter = unit.querySelector ('.linear-character');
const linearRelated = unit.querySelector ('.linear-related');
const linearCodePoint = unit.querySelector ('.linear-code-point');
const linearCodePoints = unit.querySelector ('.linear-code-points');
const detailedContainer = unit.querySelector ('.detailed-container')
const instructions = unit.querySelector ('.instructions');
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
const unihanHistorySize = 128;   // 0: unlimited
//
let unihanHistory = [ ];
let unihanHistoryIndex = -1;
let unihanHistorySave = null;
//
let currentUnihanCharacter;
//
module.exports.start = function (context)
{
    const { clipboard, shell } = require ('electron');
    const { getCurrentWindow, Menu } = require ('@electron/remote');
    //
    const currentWindow = getCurrentWindow ();
    //
    const pullDownMenus = require ('../../lib/pull-down-menus.js');
    const linksList = require ('../../lib/links-list.js');
    //
    const unicode = require ('../../lib/unicode/unicode.js');
    const unihan = require ('../../lib/unicode/unihan.js');
    const compatibilityVariants = require ('../../lib/unicode/get-cjk-compatibility-variants.js');
    //
    const { unencodedComponents } = require ('../../lib/unicode/cjk-ids.json');
    //
    const related = require ('../../lib/unicode/cjk-related.json').data;
    //
    const debugRelated = true;
    //
    if (debugRelated)
    {
        function sorted (string)
        {
            return Array.from (string).sort ((a, b) => a.codePointAt (0) - b.codePointAt (0)).join ("");
        }
        //
        function hasDuplicates (string)
        {
            let array = Array.from (string);
            return [...new Set (array)].length !== array.length;
        }
        //
        let groupCount = 0;
        let setCount = 0;
        let sets = [];
        let relatedChars = "";
        for (let category in related)
        {
            for (let group in related[category])
            {
                let setArray = related[category][group];
                if (Array.isArray (setArray))
                {
                    let relatedSets = { };
                    groupCount++;
                    for (let chars of setArray)
                    {
                        if (chars && (chars[0] !== "#"))
                        {
                            setCount++;
                            sets.push (chars);
                            if (hasDuplicates (chars))
                            {
                                console.log (`Duplicate Characters: "${chars}"`);
                            }
                            let unsortedChars = chars;
                            chars = sorted (chars);
                            if (chars in relatedSets)
                            {
                                if ((category + group) === relatedSets[chars])
                                {
                                    console.log (`Duplicate Set: "${unsortedChars}"`);
                                }
                            }
                            else
                            {
                                relatedSets[chars] = category + group;
                            }
                            // To improve... assumes prior sorting
                            chars = chars.replace (/^[*~≈]/u, "");
                            let validChars = Array.from (chars).filter (char => unihan.isUnihan (char)).join ("");
                            if (validChars !== chars)
                            {
                                console.log (`Invalid CJK Characters: "${chars}"`);
                            }
                            relatedChars += validChars;
                        }
                    }
                }
                else
                {
                    // Assume subgroups
                    for (let subgroup in setArray)
                    {
                        if (subgroup && (subgroup[0] !== '~'))
                        {
                            let relatedSets = { };
                            groupCount++;
                            for (let chars of setArray[subgroup])
                            {
                                if (chars && (chars[0] !== "#"))
                                {
                                    setCount++;
                                    sets.push (chars);
                                    if (hasDuplicates (chars))
                                    {
                                        console.log (`Duplicate Characters: "${chars}"`);
                                    }
                                    let unsortedChars = chars;
                                    chars = sorted (chars);
                                    if (chars in relatedSets)
                                    {
                                        if ((category + group + subgroup) === relatedSets[chars])
                                        {
                                            console.log (`Duplicate Set: "${unsortedChars}"`);
                                        }
                                    }
                                    else
                                    {
                                        relatedSets[chars] = category + group + subgroup;
                                    }
                                    // To improve... assumes prior sorting
                                    chars = chars.replace (/^[*~≈]/u, "");
                                    let validChars = Array.from (chars).filter (char => unihan.isUnihan (char)).join ("");
                                    if (validChars !== chars)
                                    {
                                        console.log (`Invalid CJK Characters: "${chars}"`);
                                    }
                                    relatedChars += validChars;
                                }
                            }
                        }
                    }
                }
            }
        }
        let categories = Object.keys (related);
        console.log ("Related CJK Characters:");
        console.log (`\tCategories [${categories.length}]: ${categories.sort ().map (s => JSON.stringify (s)).join (", ")}`);
        console.log ("\tGroups:", groupCount);
        console.log ("\tSets:", setCount);
        console.log ("\tCharacters:", [...new Set (Array.from (relatedChars))].length);
    }
    //
    const refLinks = require ('./ref-links.json');
    //
    const defaultPrefs =
    {
        unihanHistory: [ ],
        unihanCharacter: "",
        extrapolatedCheckbox: false,
        instructions: true,
        references: false,
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    unihanHistory = prefs.unihanHistory;
    //
    function displayLookUpData (unihanCharacter)
    {
        currentUnihanCharacter = unihanCharacter;
        if (unihanCharacter)
        {
            let indexOfUnihanCharacter = unihanHistory.indexOf (unihanCharacter);
            if (indexOfUnihanCharacter !== -1)
            {
                unihanHistory.splice (indexOfUnihanCharacter, 1);
            }
            unihanHistory.unshift (unihanCharacter);
            if ((unihanHistorySize > 0) && (unihanHistory.length > unihanHistorySize))
            {
                unihanHistory.pop ();
            }
            unihanHistoryIndex = -1;
            unihanHistorySave = null;
        }
    }
    //
    unihanInput.addEventListener
    (
        'input',
        (event) =>
        {
            event.currentTarget.classList.remove ('invalid');
            if (event.currentTarget.value)
            {
                if (!unihan.validateUnihanInput (event.currentTarget.value))
                {
                    event.currentTarget.classList.add ('invalid');
                }
            }
        }
    );
    unihanInput.addEventListener
    (
        'keypress',
        (event) =>
        {
            if (event.key === 'Enter')
            {
                event.preventDefault ();
                lookUpButton.click ();
            }
        }
    );
    unihanInput.addEventListener
    (
        'keydown',
        (event) =>
        {
            if (event.altKey)
            {
                if (event.key === 'ArrowUp')
                {
                    event.preventDefault ();
                    if (unihanHistoryIndex === -1)
                    {
                        unihanHistorySave = event.currentTarget.value;
                    }
                    unihanHistoryIndex++;
                    if (unihanHistoryIndex > (unihanHistory.length - 1))
                    {
                        unihanHistoryIndex = (unihanHistory.length - 1);
                    }
                    if (unihanHistoryIndex !== -1)
                    {
                        event.currentTarget.value = unihanHistory[unihanHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
                else if (event.key === 'ArrowDown')
                {
                    event.preventDefault ();
                    unihanHistoryIndex--;
                    if (unihanHistoryIndex < -1)
                    {
                        unihanHistoryIndex = -1;
                        unihanHistorySave = null;
                    }
                    if (unihanHistoryIndex === -1)
                    {
                        if (unihanHistorySave !== null)
                        {
                            event.currentTarget.value = unihanHistorySave;
                            event.currentTarget.dispatchEvent (new Event ('input'));
                        }
                    }
                    else
                    {
                        event.currentTarget.value = unihanHistory[unihanHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
            }
        }
    );
    //
    function relatedCandidates (character)
    {
        let candidates = [ ];
        //
        if (unihan.isCompatibility (character))
        {
            let unified = character.normalize ('NFD');
            let compatibility = compatibilityVariants[unified] || [ ];
            if (compatibility.length > 0)
            {
                candidates.push ({ category: "Compatibility", group: unified, chars: unified + compatibility.join ("") });
            }
        }
        else
        {
            let compatibility = compatibilityVariants[character] || [ ];
            if (compatibility.length > 0)
            {
                candidates.push ({ category: "Compatibility", group: character, chars: character + compatibility.join ("") });
            }
        }
        //
        for (let category in related)
        {
            if (category && (category[0] !== '~'))
            {
                for (let group in related[category])
                {
                    if (group && (group[0] !== '~'))
                    {
                        let setArray = related[category][group];
                        if (Array.isArray (setArray))
                        {
                            for (let chars of setArray)
                            {
                                if (chars && (chars[0] !== '#') && (chars[0] !== '~'))
                                {
                                    let marker = "";
                                    let found = chars.match (/([*≈])$/u);
                                    if (found)
                                    {
                                        marker = found[1];
                                    }
                                    let charArray = Array.from (chars);
                                    if (charArray.length > 1)
                                    {
                                        chars = charArray.filter (char => unihan.isUnihan (char)).join ("");
                                        if ((chars.indexOf (character) > -1) || (chars.indexOf (character.normalize ('NFD')) > -1))
                                        {
                                            candidates.push ({ category, group, marker, chars });
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Assume subgroups
                            for (let subgroup in setArray)
                            {
                                if (subgroup && (subgroup[0] !== '~'))
                                {
                                    for (let chars of setArray[subgroup])
                                    {
                                        if (chars && (chars[0] !== '#') && (chars[0] !== '~'))
                                        {
                                            let marker = "";
                                            let found = chars.match (/([*≈])$/u);
                                            if (found)
                                            {
                                                marker = found[1];
                                            }
                                            let charArray = Array.from (chars);
                                            if (charArray.length > 1)
                                            {
                                                chars = charArray.filter (char => unihan.isUnihan (char)).join ("");
                                                if ((chars.indexOf (character) > -1) || (chars.indexOf (character.normalize ('NFD')) > -1))
                                                {
                                                    candidates.push ({ category, group, subgroup, marker, chars });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //
        return candidates;
    }
    //
    let currentCharacter;
    //
    let characterMenuTemplate =
    [
        { label: "Copy Character", click: (menuItem) => clipboard.writeText (currentCharacter) },
        { label: "Copy Code Point", click: (menuItem) => clipboard.writeText (unicode.characterToCodePoint (currentCharacter)) }
    ];
    let characterContextualMenu = Menu.buildFromTemplate (characterMenuTemplate);
    //
    function showCharacterMenu (event)
    {
        let link = event.target.closest ('.unihan-character-link');
        if (link)
        {
            event.preventDefault ();
            currentCharacter = link.dataset.char || link.textContent;
            characterContextualMenu.popup ({ window: currentWindow });
        }
    }
    //
    function onLinkClick (event)
    {
        let clickable = event.target.closest ('.clickable');
        if (clickable)
        {
            event.preventDefault ();
            updateUnihanData (clickable.dataset.char);
        }
    }
    //
    let currentSet;
    //
    let copySetMenuTemplate =
    [
        { label: "Copy Set", click: (menuItem) => clipboard.writeText (currentSet) }
    ];
    let copySetContextualMenu = Menu.buildFromTemplate (copySetMenuTemplate);
    //
    function showCopySetMenu (event)
    {
        let set = event.target.closest ('.detailed-data');
        if (set)
        {
            event.preventDefault ();
            currentSet = set.dataset.set;
            copySetContextualMenu.popup ({ window: currentWindow });
        }
    }
    //
    function isExtrapolatedSet (chars)
    {
        return /\*$/.test (chars);
    }
    //
    function getCodePoint (character)
    {
        return character.codePointAt (0);
    }
    //
    const totalStrokesSortFunc = (a, b) => (unihan.getTotalStrokes (a) - unihan.getTotalStrokes (b)) || (getCodePoint (a) - getCodePoint (b));
    //
    function getNameTooltip (character)
    {
        let tooltip = Array.from (character).map
        (
            char =>
            {
                let data = unicode.getCharacterBasicData (char);
                return `<${data.codePoint}>\xA0${(data.name === "<control>") ? data.alias : data.name}`;
            }
        ).join (",\n");
        return tooltip;
    }
    //
    function updateUnihanData (character)
    {
        while (linearCharacter.firstChild)
        {
            linearCharacter.firstChild.remove ();
        }
        // linearRelated.textContent = "";
        while (linearRelated.firstChild)
        {
            linearRelated.firstChild.remove ();
        }
        linearCodePoint.textContent = "";
        linearCodePoints.textContent = "";
        while (detailedContainer.firstChild)
        {
            detailedContainer.firstChild.remove ();
        }
        if (character)
        {
            let [ base, vs ] = character;   // Might allow IVS input in the future...
            character = base;
            let mergedRelated = "";
            for (let category in related)
            {
                if (category && (category[0] !== '~'))
                {
                    for (let group in related[category])
                    {
                        if (group && (group[0] !== '~'))
                        {
                            let setArray = related[category][group];
                            if (Array.isArray (setArray))
                            {
                                for (let chars of setArray)
                                {
                                    if (chars && (chars[0] !== '#') && (chars[0] !== '~'))
                                    {
                                        if ((!isExtrapolatedSet (chars) || extrapolatedCheckbox.checked))
                                        {
                                            chars = Array.from (chars).filter (char => unihan.isUnihan (char)).join ("");
                                            if ((chars.indexOf (character) > -1) || (chars.indexOf (character.normalize ('NFD')) > -1))
                                            {
                                                mergedRelated += chars;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Assume subgroups
                                for (let subgroup in setArray)
                                {
                                    if (subgroup && (subgroup[0] !== '~'))
                                    {
                                        for (let chars of setArray[subgroup])
                                        {
                                            if (chars && (chars[0] !== '#') && (chars[0] !== '~'))
                                            {
                                                if ((!isExtrapolatedSet (chars) || extrapolatedCheckbox.checked))
                                                {
                                                    chars = Array.from (chars).filter (char => unihan.isUnihan (char)).join ("");
                                                    if ((chars.indexOf (character) > -1) || (chars.indexOf (character.normalize ('NFD')) > -1))
                                                    {
                                                        mergedRelated += chars;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (unihan.isCompatibility (character))
            {
                let unified = character.normalize ('NFD');
                mergedRelated += unified;
                let compatibility = compatibilityVariants[unified] || [ ];
                mergedRelated += compatibility.join ("");
            }
            else
            {
                let compatibility = compatibilityVariants[character] || [ ];
                mergedRelated += compatibility.join ("");
            }
            let sortedChars = [...new Set (Array.from (mergedRelated).filter (char => (char !== character)))].sort (totalStrokesSortFunc);
            //
            currentUnihanCharacter = character;
            if (character)
            {
                let indexOfUnihanCharacter = unihanHistory.indexOf (character);
                if (indexOfUnihanCharacter !== -1)
                {
                    unihanHistory.splice (indexOfUnihanCharacter, 1);
                }
                unihanHistory.unshift (character);
                if ((unihanHistorySize > 0) && (unihanHistory.length > unihanHistorySize))
                {
                    unihanHistory.pop ();
                }
                unihanHistoryIndex = -1;
                unihanHistorySave = null;
                let symbol = document.createElement ('span');
                symbol.className = 'symbol';
                symbol.classList.add ('unihan-character-link');
                symbol.title = unihan.getExpandedTooltip (character);
                symbol.textContent = character;
                linearCharacter.appendChild (symbol);
                linearCharacter.addEventListener ('contextmenu', showCharacterMenu);
                let codePoint = document.createElement ('span');
                codePoint.className = 'code-point';
                if (unihan.isCompatibility (character))
                {
                    codePoint.classList.add ('compatibility');
                }
                codePoint.title = getNameTooltip (character);
                codePoint.textContent = unicode.characterToCodePoint (character);
                linearCodePoint.appendChild (codePoint);
                if (sortedChars.length > 0)
                {
                    let isFirst = true;
                    for (let sortedChar of sortedChars)
                    {
                        let symbol = document.createElement ('span');
                        symbol.className = 'symbol';
                        symbol.classList.add ('unihan-character-link');
                        symbol.classList.add ('clickable');
                        symbol.dataset.char = sortedChar;
                        symbol.title = unihan.getExpandedTooltip (sortedChar);
                        symbol.textContent = sortedChar;
                        linearRelated.appendChild (symbol);
                        if (!isFirst)
                        {
                            linearCodePoints.appendChild (document.createTextNode (" "));
                        }
                        isFirst = false;
                        let codePoint = document.createElement ('span');
                        codePoint.className = 'code-point';
                        if (unihan.isCompatibility (sortedChar))
                        {
                            codePoint.classList.add ('compatibility');
                        }
                        codePoint.title = getNameTooltip (sortedChar);
                        codePoint.textContent = unicode.characterToCodePoint (sortedChar);
                        linearCodePoints.appendChild (codePoint);
                    }
                    linearRelated.addEventListener ('click', onLinkClick);
                    linearRelated.addEventListener ('contextmenu', showCharacterMenu);
                    detailedContainer.appendChild (createDetailedTable (character, relatedCandidates (character)));
                    detailedContainer.addEventListener ('contextmenu', showCharacterMenu);
                    detailedContainer.addEventListener ('contextmenu', showCopySetMenu);
                }
            }
        }
        unihanInput.value = "";
        unihanInput.blur ();
        unihanInput.dispatchEvent (new Event ('input'));
        displayLookUpData (character);
        unit.scrollTop = 0;
        unit.scrollLeft = 0;
    }
    //
    function getTooltip (character)
    {
        let data = unicode.getCharacterBasicData (character);
        return `${data.codePoint.replace (/U\+/, "U\u034F\+")}\xA0${character}` + (unihan.isRadical (character) ? " (Radical)" : ""); // U+034F COMBINING GRAPHEME JOINER
    }
    //
    function isWideChar (char)
    {
        let isWide =
        unicode.matchEastAsianWidth (char, [ 'F', 'W' ])
        ||
        (
            unicode.matchEastAsianWidth (char, [ 'A' ])
            &&
            unicode.matchVerticalOrientation (char, [ 'U', 'Tu' ])
        );
        return isWide;
    }
    //
    function appendTextWithLinks (node, text, character)
    {
        for (let char of text)
        {
            if (unihan.isUnihan (char))
            {
                let link = document.createElement ('span');
                link.className = 'unihan-character-link';
                if (char !== character)
                {
                    link.classList.add ('clickable');
                    link.dataset.char = char;
                }
                link.textContent = char;
                link.title = unihan.getExpandedTooltip (char);
                node.appendChild (link);
            }
            else if (unihan.isRadical (char))
            {
                let radical = document.createElement ('span');
                radical.className = 'radical';
                radical.textContent = char;
                radical.title = getTooltip (char);
                node.appendChild (radical);
            }
            else if (char in unencodedComponents)
            {
                let unencoded = document.createElement ('span');
                unencoded.className = 'unencoded';
                unencoded.textContent = char;
                let description = unencodedComponents[char];
                unencoded.title = `Unencoded Component:\n${unicode.characterToCodePoint (char)}\xA0(${description})`;
                node.appendChild (unencoded);
            }
            else if (isWideChar (char))
            {
                let other = document.createElement ('span');
                other.className = 'wide-character';
                other.textContent = char;
                node.appendChild (other);
            }
            else
            {
                node.appendChild (document.createTextNode (char));
            }
        }
        node.normalize ();
        node.addEventListener ('click', onLinkClick);
    }
    //
    let groupSeparator = "\xA0:\xA0";
    // let groupSeparator = "：";
    // let groupSeparator = "\xA0‣\xA0";
    // let groupSeparator = "\xA0⏵\xA0";
    // let groupSeparator = "\xA0➢\xA0";
    //
    function createDetailedTable (character, candidates)
    {
        let detailedList = document.createElement ('table');
        detailedList.className = 'detailed-list';
        for (let candidate of candidates)
        {
            if ((candidate.marker !== "*") || extrapolatedCheckbox.checked)
            {
                let detailedHeaderRow = document.createElement ('tr');
                detailedHeaderRow.className = 'detailed-header-row';
                let detailedHeader = document.createElement ('th');
                detailedHeader.className = 'detailed-header';
                if (candidate.marker)
                {
                    detailedHeader.classList.add ('marked');
                    let markerPrefix = document.createElement ('span');
                    markerPrefix.className = 'marker';
                    markerPrefix.textContent = `(${candidate.marker})\xA0\xA0`;
                    detailedHeader.appendChild (markerPrefix);
                }
                let detailedCategory = document.createElement ('span');
                detailedCategory.className = 'category';
                detailedCategory.textContent = candidate.category;
                detailedHeader.appendChild (detailedCategory);
                let detailedSeparator = document.createElement ('span');
                detailedSeparator.className = 'separator';
                detailedSeparator.textContent = groupSeparator;
                detailedHeader.appendChild (detailedSeparator);
                let detailedGroup = document.createElement ('span');
                detailedGroup.className = 'group';
                appendTextWithLinks (detailedGroup, candidate.group, character);
                detailedHeader.appendChild (detailedGroup);
                if (candidate.subgroup)
                {
                    detailedHeader.appendChild (detailedSeparator.cloneNode (true));
                    let detailedSubgroup = document.createElement ('span');
                    detailedSubgroup.className = 'subgroup';
                    appendTextWithLinks (detailedSubgroup, candidate.subgroup, character);
                    detailedHeader.appendChild (detailedSubgroup);
                }
                detailedHeaderRow.appendChild (detailedHeader);
                detailedList.appendChild (detailedHeaderRow);
                let detailedDataRow = document.createElement ('tr');
                detailedDataRow.className = 'detailed-data-row';
                let detailedData = document.createElement ('td');
                detailedData.className = 'detailed-data';
                detailedData.dataset.set = candidate.chars;
                for (let char of candidate.chars)
                {
                    let combo = document.createElement ('div');
                    combo.className = 'combo';
                    let link = document.createElement ('span');
                    link.className = 'unihan-character-link';
                    if (char != character)
                    {
                        link.classList.add ('clickable');
                        link.dataset.char = char;
                    }
                    link.textContent = char;
                    link.title = unihan.getExpandedTooltip (char);
                    combo.appendChild (link);
                    let codePoint = document.createElement ('span');
                    codePoint.className = 'code-point';
                    if (unihan.isCompatibility (char))
                    {
                        codePoint.classList.add ('compatibility');
                    }
                    codePoint.textContent = unicode.characterToCodePoint (char, true);
                    combo.appendChild (codePoint);
                    detailedData.appendChild (combo);
                }
                detailedData.addEventListener ('click', onLinkClick);
                detailedDataRow.appendChild (detailedData);
                detailedList.appendChild (detailedDataRow);
            }
        }
        return detailedList;
    }
    //
    lookUpButton.addEventListener
    (
        'click',
        (event) =>
        {
            if (unihanInput.value)
            {
                let character = unihan.validateUnihanInput (unihanInput.value);
                if (character)
                {
                    updateUnihanData (character);
                }
                else
                {
                    shell.beep ();
                }
            }
            else
            {
                unihanHistoryIndex = -1;
                unihanHistorySave = null;
                updateUnihanData ("");
            }
        }
    );
    //
    const textSeparator = (process.platform === 'darwin') ? "\t" : "\xA0\xA0";
    //
    function insertUnihanCharacter (menuItem)
    {
        unihanInput.value = menuItem.id;
        unihanInput.dispatchEvent (new Event ('input'));
        lookUpButton.click ();
    };
    historyButton.addEventListener
    (
        'click',
        (event) =>
        {
            let historyMenuTemplate = [ ];
            historyMenuTemplate.push ({ label: "Lookup History", enabled: false })
            // historyMenuTemplate.push ({ type: 'separator' })
            if (unihanHistory.length > 0)
            {
                for (let unihan of unihanHistory)
                {
                    historyMenuTemplate.push
                    (
                        {
                            label: `${unihan}${textSeparator}${unicode.characterToCodePoint (unihan)}`,
                            id: unihan,
                            toolTip: unicode.getCharacterBasicData (unihan).name,
                            click: insertUnihanCharacter
                        }
                    );
                }
            }
            else
            {
                historyMenuTemplate.push ({ label: "(no history yet)", enabled: false });
            }
            let historyContextualMenu = Menu.buildFromTemplate (historyMenuTemplate);
            pullDownMenus.popup (event.currentTarget, historyContextualMenu, 0);
        }
    );
    //
    extrapolatedCheckbox.checked = prefs.extrapolatedCheckbox;
    extrapolatedCheckbox.addEventListener ('click', (event) => { updateUnihanData (currentUnihanCharacter); });
    //
    currentUnihanCharacter = prefs.unihanCharacter;
    updateUnihanData (currentUnihanCharacter);
    //
    instructions.open = prefs.instructions;
    //
    references.open = prefs.references;
    //
    linksList (links, refLinks);
    //
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        unihanHistory: unihanHistory,
        unihanCharacter: currentUnihanCharacter,
        extrapolatedCheckbox: extrapolatedCheckbox.checked,
        instructions: instructions.open,
        references: references.open
    };
    context.setPrefs (prefs);
};
//
