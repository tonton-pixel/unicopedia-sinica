//
const unitId = 'cjk-sources-unit';
//
const unit = document.getElementById (unitId);
//
const clearButton = unit.querySelector ('.clear-button');
const charactersSamples = unit.querySelector ('.characters-samples');
const countInfo = unit.querySelector ('.count-info');
const countNumber = unit.querySelector ('.count-number');
const totalCountNumber = unit.querySelector ('.total-count-number');
const loadButton = unit.querySelector ('.load-button');
const saveButton = unit.querySelector ('.save-button');
const charactersInput = unit.querySelector ('.characters-input');
const codePointsInput = unit.querySelector ('.code-points-input');
const sheet = unit.querySelector ('.sheet');
const statistics = unit.querySelector ('.statistics');
const statisticsTable = unit.querySelector ('.statistics-table');
const irgSourceRefs = unit.querySelector ('.irg-source-refs');
const irgSourcesTable = unit.querySelector ('.irg-sources-table');
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
const instructions = unit.querySelector ('.instructions');
//
let defaultFolderPath;
//
module.exports.start = function (context)
{
    const { clipboard } = require ('electron');
    const { app, getCurrentWindow, Menu } = require ('@electron/remote');
    //
    const userDataPath = app.getPath ('userData');
    //
    const currentWindow = getCurrentWindow ();
    //
    const fs = require ('fs');
    const path = require ('path');
    //
    const pullDownMenus = require ('../../lib/pull-down-menus.js');
    const sampleMenus = require ('../../lib/sample-menus.js');
    const fileDialogs = require ('../../lib/file-dialogs.js');
    const linksList = require ('../../lib/links-list.js');
    //
    const unicode = require ('../../lib/unicode/unicode.js');
    const unihan = require ('../../lib/unicode/unihan.js');
    const { codePoints } = require ('../../lib/unicode/parsed-unihan-data.js');
    //
    const pages = require ('./svg-pages-16.0.json');
    //
    const refLinks = require ('./ref-links.json');
    //
    const defaultPrefs =
    {
        charactersInput: "",
        instructions: true,
        statistics: false,
        irgSourceRefs: false,
        references: false,
        defaultFolderPath: context.defaultFolderPath
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    clearButton.addEventListener
    (
        'click',
        event =>
        {
            charactersInput.value = "";
            charactersInput.dispatchEvent (new Event ('input'));
            charactersInput.focus ();
        }
    );
    //
    const samples = require ('./samples.json');
    //
    samples.push (null);    // Separator
    //
    const unifiedBlockNames =
    {
        "CJK Unified (URO)": "U4E00",
        "CJK Unified Extension A": "U3400",
        "CJK Unified Extension B": "U20000",
        "CJK Unified Extension C": "U2A700",
        "CJK Unified Extension D": "U2B740",
        "CJK Unified Extension E": "U2B820",
        "CJK Unified Extension F": "U2CEB0",
        "CJK Unified Extension G": "U30000",
        "CJK Unified Extension H": "U31350",
        "CJK Unified Extension I": "U2EBF0"
    };
    //
    let unifiedRangeSamples = [ ];
    for (let blockName in unifiedBlockNames)
    {
        let blockRanges = [ ];
        for (page of pages)
        {
            if (page.block === unifiedBlockNames[blockName])
            {
                let characters = [ ];
                for (let value = page.first; value <= page.last; value++)
                {
                    characters.push (String.fromCodePoint (value));
                }
                blockRanges.push ({ label: page.range.replaceAll ("U", "U+"), string: characters.join ("") });
            }
        }
        unifiedRangeSamples.push ({ label: blockName, items: blockRanges });
    }
    //
    let misclassifiedSamples =
    [
        {
            "label": "CJK Unified (Misclassified)",
            "string": "﨎﨏﨑﨓﨔﨟﨡﨣﨤﨧﨨﨩"
        }
    ];
    //
    const compatibilityBlockNames =
    {
        "CJK Compatibility": "UF900",
        "CJK Compat. Supplement": "U2F800"
    };
    //
    let compatibilityRangeSamples = [ ];
    for (let blockName in compatibilityBlockNames)
    {
        let blockRanges = [ ];
        for (page of pages)
        {
            if (page.block === compatibilityBlockNames[blockName])
            {
                let characters = [ ];
                for (let value = page.first; value <= page.last; value++)
                {
                    characters.push (String.fromCodePoint (value));
                }
                blockRanges.push ({ label: page.range.replaceAll ("U", "U+"), string: characters.join ("") });
            }
        }
        compatibilityRangeSamples.push ({ label: blockName, items: blockRanges });
    }
    //
    let charactersMenu = sampleMenus.makeMenu
    (
        [...samples, ...unifiedRangeSamples, ...misclassifiedSamples, ...compatibilityRangeSamples],
        (sample) =>
        {
            charactersInput.value = sample.string;
            charactersInput.dispatchEvent (new Event ('input'));
        }
    );
    //
    charactersSamples.addEventListener
    (
        'click',
        event =>
        {
            pullDownMenus.popup (event.currentTarget, charactersMenu);
        }
    );
    //
    defaultFolderPath = prefs.defaultFolderPath;
    //
    loadButton.addEventListener
    (
        'click',
        event =>
        {
            fileDialogs.loadTextFile
            (
                "Load text file:",
                [ { name: "Text (*.txt)", extensions: [ 'txt' ] } ],
                defaultFolderPath,
                'utf8',
                (text, filePath) =>
                {
                    let maxLength = charactersInput.maxLength;
                    if (text.length > maxLength)
                    {
                        if (/[\uD800-\uDBFF]/.test (text[maxLength - 1]))   // Unpaired high surrogate
                        {
                            maxLength = maxLength - 1;
                        }
                        text = text.substring (0, maxLength);
                    }
                    charactersInput.value = text;
                    charactersInput.dispatchEvent (new Event ('input'));
                    defaultFolderPath = path.dirname (filePath);
                }
            );
        }
    );
    //
    saveButton.addEventListener
    (
        'click',
        event =>
        {
            fileDialogs.saveTextFile
            (
                "Save text file:",
                [ { name: "Text (*.txt)", extensions: [ 'txt' ] } ],
                defaultFolderPath,
                (filePath) =>
                {
                    defaultFolderPath = path.dirname (filePath);
                    return charactersInput.value;
                }
            );
        }
    );
    //
    const sources =
    {
        "G":
        {
            property: "kIRG_GSource",
            source: "China",
            designation: "G",
            modifier: "🄖"
        },
        "H":
        {
            property: "kIRG_HSource",
            source: "Hong Kong",
            designation: "H",
            modifier: "🄗"
        },
        "M":
        {
            property: "kIRG_MSource",
            source: "Macao",
            designation: "M",
            modifier: "🄜"
        },
        "T":
        {
            property: "kIRG_TSource",
            source: "Taiwan",
            designation: "T",
            modifier: "🄣"
        },
        "J":
        {
            property: "kIRG_JSource",
            source: "Japan",
            designation: "J",
            modifier: "🄙"
        },
        "K":
        {
            property: "kIRG_KSource",
            source: "South Korea",
            designation: "K",
            modifier: "🄚"
        },
        "KP":
        {
            property: "kIRG_KPSource",
            source: "North Korea",
            designation: "P",
            modifier: "🄟"
        },
        "V":
        {
            property: "kIRG_VSource",
            source: "Vietnam",
            designation: "V",
            modifier: "🄥"
        },
        "UTC":
        {
            property: "kIRG_USource",
            source: "UTC",
            designation: "U",
            modifier: "🄤"
        },
        "SAT":
        {
            property: "kIRG_SSource",
            source: "SAT",
            designation: "S",
            modifier: "🄢"
        },
        "UK":
        {
            property: "kIRG_UKSource",
            source: "U.K.",
            designation: "B",
            modifier: "🄑"
        }
    };
    //
    const sortedPrefixes = Object.keys (sources).reverse ();
    //
    function toDesignation (string)
    {
        let designation = "";
        for (let prefix of sortedPrefixes)
        {
            if (string.startsWith (prefix))
            {
                designation = sources[prefix].designation;
                break;
            }
        }
        return designation;
    }
    //
    const legacyPosition = 'last'; // 'first', 'last', 'none'
    //
    const legacySource =
    {
        // No Unihan property, extension B only
        prefix: "UCS",
        source: "UCS2003",
        title: "Legacy",
        designation: "Q",
        modifier: "🄠"
    };
    //
    const adjustSizes = false;
    //
    const showLocalRendering = true;
    //
    const useFooter = true;
    //
    function getBlockRange (character)
    {
        let result = null;
        let value = character.codePointAt (0);
        for (let page of pages)
        {
            if ((value >= page.first) && (value <= page.last))
            {
                result = { block: page.block, range: page.range, page: page.page };
                break;
            }
        }
        return result;
    }
    //
    function getLegacyBlockRange (character)
    {
        let result = null;
        let block = "U20000-UCS2003";
        let value = character.codePointAt (0);
        let rangeIndex = Math.floor (value / 256);
        let min = (rangeIndex * 256);
        let max = min + (256 - 1);
        let range = `U${min.toString (16).toUpperCase ()}..U${max.toString (16).toUpperCase ()}`;
        let page = 1 + 1 + rangeIndex - (0x20000 / 256);
        result = { block, range, page };
        return result;
    }
    //
    function createHeader (text, title, isLegacy)
    {
        let header = document.createElement ('th');
        header.className = 'source';
        if (isLegacy)
        {
            header.classList.add ('legacy');
        }
        header.textContent = text;
        header.title = title;
        return header;
    }
    // Hard-coded, version-dependent!!
    let blockPages =
    {
        "U3400": 370,
        "U4E00": 551,
        "UF900": 1228,
        "U20000": 1883,
        "U2A700": 2717,
        "U2B740": 2770,
        "U2B820": 2773,
        "U2CEB0": 2846,
        "U2EBF0": 2940,
        "U2F800": 2948,
        "U30000": 2966,
        "U31350": 3028
    }
    //
    function createData (character, svgExists, svgPath, source, isLegacy)
    {
        let data = document.createElement ('td');
        data.className = 'data';
        let sourceReference = null;
        if (isLegacy)
        {
            if (/[\u{20000}-\u{2A6D6}]/u.test (character))  // Extension B with UCS2003
            {
                sourceReference = legacySource.source;
            }
        }
        else
        {
            let codePoint = unicode.characterToCodePoint (character);
            if (source.property in codePoints[codePoint])
            {
                sourceReference = codePoints[codePoint][source.property];
            }
        }
        if (sourceReference)
        {
            let glyph = document.createElement ('div');
            glyph.className = 'glyph';
            let code = document.createElement ('div');
            code.className = 'source-code';
            let { block, range, page } = (isLegacy) ? getLegacyBlockRange (character) : getBlockRange (character);
            let id = (isLegacy) ? `${character}_${legacySource.designation}` : `${character}_${toDesignation (sourceReference)}`;
            let link;
            if (block === "U20000-UCS2003")
            {
                link = `https://www.unicode.org/Public/13.0.0/charts/UCS2003.pdf#page=${page}`;
            }
            else
            {
                // link = `https://www.unicode.org/charts/PDF/${block}.pdf#page=${page}`;
                link = `https://www.unicode.org/Public/16.0.0/charts/CodeCharts.pdf#page=${blockPages[block] + page - 2}`;
            }
            glyph.dataset.char = character;
            glyph.dataset.link = link;
            glyph.title = link;
            if (svgExists)
            {
                const xmlns = "http://www.w3.org/2000/svg";
                let svg = document.createElementNS (xmlns, 'svg');
                if (adjustSizes && /_U$/.test (id))
                {
                    // Visual size adjustment (upscaling)
                    svg.setAttributeNS (null, 'class', 'svg-glyph utc-source');
                }
                else if (adjustSizes && /_V$/.test (id))
                {
                    // Visual size adjustment (downscaling)
                    svg.setAttributeNS (null, 'class', 'svg-glyph v-source');
                }
                else
                {
                    svg.setAttributeNS (null, 'class', 'svg-glyph');
                }
                let use = document.createElementNS (xmlns, 'use');
                use.setAttributeNS (null, 'href', path.join (svgPath, block, `${range}.svg#${id}`));
                svg.appendChild (use);
                glyph.appendChild (svg);
            }
            else
            {
                // Display PDF chart file name followed by page number (URL-like)
                let pdfInfo = document.createElement ('div');
                pdfInfo.className = 'pdf-info';
                let pdfName = document.createElement ('span');
                // pdfName.textContent = (isLegacy) ? "UCS2003.pdf" : `${block}.pdf`;
                pdfName.textContent = (isLegacy) ? "UCS2003.pdf" : "CodeCharts.pdf";
                pdfInfo.appendChild (pdfName);
                let wbr = document.createElement ('wbr');
                pdfInfo.appendChild (wbr);
                let pdfPage = document.createElement ('span');
                // pdfPage.textContent = `#page=${page}`;
                pdfPage.textContent = (isLegacy) ? `#page=${page}` : `#page=${blockPages[block] + page - 2}`;
                pdfInfo.appendChild (pdfPage);
                glyph.appendChild (pdfInfo);
            }
            code.textContent = sourceReference;
            data.appendChild (glyph);
            data.appendChild (code);
        }
        else
        {
            data.classList.add ('empty');
        }
        return data;
    }
    //
    let currentChar;
    let currentLink;
    //
    let sourceMenuTemplate =
    [
        { label: "Copy Character", click: (menuItem) => clipboard.writeText (currentChar) },
        { label: "Copy Code Point", click: (menuItem) => clipboard.writeText (unicode.characterToCodePoint (currentChar)) },
        { type: 'separator' },
        { label: "Copy Reference Link", click: (menuItem) => clipboard.writeText (currentLink) }
    ];
    let sourceContextualMenu = Menu.buildFromTemplate (sourceMenuTemplate);
    //
    function showSourceMenu (event)
    {
        let glyph = event.target.closest ('div[class="glyph"]');
        if (glyph)
        {
            event.preventDefault ();
            let char = glyph.dataset.char;
            let link = glyph.dataset.link;
            if (char && link)
            {
                currentChar = char;
                currentLink = link;
                sourceContextualMenu.popup ({ window: currentWindow });
            }
        }
    }
    //
    function createSheet (unihanCharacters)
    {
        while (sheet.firstChild)
        {
            sheet.firstChild.remove ();
        }
        if (unihanCharacters.length > 0)
        {
            let svgDataPath = path.join (userDataPath, 'svg-glyphs-16.0');
            let svgDataExists = fs.existsSync (svgDataPath);
            for (let prefix in sources)
            {
                let source = sources[prefix];
                let count = 0;
                let legacyCount = 0;
                for (let character of unihanCharacters)
                {
                    if (legacyPosition !== 'none')
                    {
                        if (/[\u{20000}-\u{2A6D6}]/u.test (character))  // Extension B with UCS2003
                        {
                            legacyCount++;
                        }
                    }
                    let codePoint = unicode.characterToCodePoint (character);
                    if (source.property)
                    {
                        if (source.property in codePoints[codePoint])
                        {
                            count++;
                        }
                    }
                }
                source.hidden = (count === 0);
                legacySource.hidden = (legacyCount === 0);
            }
            let table = document.createElement ('table');
            table.className= 'table';
            let headerRow = document.createElement ('tr');
            if (showLocalRendering)
            {
                let localHeader = document.createElement ('th');
                localHeader.className = 'local';
                localHeader.textContent = "Local Font";   // "Local Glyph"
                localHeader.title = "Default local glyph rendering for reference";
                headerRow.appendChild (localHeader);
                let emptyGap = document.createElement ('th');
                emptyGap.className = 'empty-gap';
                emptyGap.textContent = "\xA0";
                headerRow.appendChild (emptyGap);
            }
            if (legacyPosition === 'first')
            {
                if (!legacySource.hidden)
                {
                    headerRow.appendChild (createHeader (legacySource.source, legacySource.title, true));
                }
            }
            for (let prefix in sources)
            {
                let source = sources[prefix];
                if (!source.hidden)
                {
                    headerRow.appendChild (createHeader (`${prefix}-Source`, source.source));
                }
            }
            if (legacyPosition === 'last')
            {
                if (!legacySource.hidden)
                {
                    headerRow.appendChild (createHeader (legacySource.source, legacySource.title, true));
                }
            }
            table.appendChild (headerRow);
            //
            for (let character of unihanCharacters)
            {
                let dataRow = document.createElement ('tr');
                dataRow.className = 'data-row';
                if (unihan.isCompatibility (character))
                {
                    dataRow.classList.add ('compatibility');
                }
                if (showLocalRendering)
                {
                    let data = document.createElement ('td');
                    data.className = 'data';
                    let glyph = document.createElement ('div');
                    glyph.className = 'glyph';
                    let fontGlyph = document.createElement ('div');
                    fontGlyph.className = 'font-glyph';
                    fontGlyph.textContent = character;
                    let code = document.createElement ('div');
                    code.className = 'code-point';
                    let codePoint = unicode.characterToCodePoint (character);
                    code.textContent = codePoint;
                    glyph.appendChild (fontGlyph);
                    data.appendChild (glyph);
                    data.appendChild (code);
                    data.title = unihan.getExpandedTooltip (character);
                    dataRow.appendChild (data);
                    let emptyGap = document.createElement ('td');
                    emptyGap.className = 'empty-gap';
                    emptyGap.textContent = "\xA0";
                    dataRow.appendChild (emptyGap);
                }
                if (legacyPosition === 'first')
                {
                    if (!legacySource.hidden)
                    {
                        dataRow.appendChild (createData (character, svgDataExists, svgDataPath, null, true));
                    }
                }
                for (let prefix in sources)
                {
                    let source = sources[prefix];
                    if (!source.hidden)
                    {
                        dataRow.appendChild (createData (character, svgDataExists, svgDataPath, source));
                    }
                }
                if (legacyPosition === 'last')
                {
                    if (!legacySource.hidden)
                    {
                        dataRow.appendChild (createData (character, svgDataExists, svgDataPath, null, true));
                    }
                }
                table.appendChild (dataRow);
            }
            if (useFooter)
            {
                table.appendChild (headerRow.cloneNode (true));
            }
            table.addEventListener ('contextmenu', showSourceMenu);
            sheet.appendChild (table);
        }
    }
    //
    let currentCharacters = "";
    //
    // Discard unsupported characters, or duplicate characters when modifier key pressed as well
    countInfo.addEventListener
    (
        'dblclick',
        event =>
        {
            if (event.altKey || event.shiftKey)
            {
                let uniqueCharacters = [...new Set (Array.from (charactersInput.value))].join ("");
                if (uniqueCharacters !== charactersInput.value)
                {
                    charactersInput.value = uniqueCharacters;
                    charactersInput.dispatchEvent (new Event ('input'));
                }
            }
            else
            {
                if (currentCharacters !== charactersInput.value)
                {
                    countNumber.textContent = Array.from (currentCharacters).length;
                    totalCountNumber.textContent = Array.from (currentCharacters).length;
                    charactersInput.value = currentCharacters;
                    codePointsInput.value = unicode.charactersToCodePoints (currentCharacters, true);
                }
            }
        }
    );
    //
    charactersInput.addEventListener
    (
        'input',
        event =>
        {
            let characters = event.currentTarget.value;
            let characterArray = Array.from (characters);
            totalCountNumber.textContent = characterArray.length;
            codePointsInput.value = unicode.charactersToCodePoints (characters, true);
            let validCharacterArray = characterArray.filter (character => unihan.isUnihan (character));
            countNumber.textContent = validCharacterArray.length;
            let validCharacters = validCharacterArray.join ("");
            if (validCharacters !== currentCharacters)
            {
                createSheet (validCharacters);
                currentCharacters = validCharacters;
            }
        }
    );
    charactersInput.value = prefs.charactersInput;
    charactersInput.dispatchEvent (new Event ('input'));
    //
    codePointsInput.addEventListener
    (
        'input',
        event =>
        {
            let characters = unicode.codePointsToCharacters (event.currentTarget.value);
            let characterArray = Array.from (characters);
            totalCountNumber.textContent = characterArray.length;
            charactersInput.value = characters;
            let validCharacterArray = characterArray.filter (character => unihan.isUnihan (character));
            countNumber.textContent = validCharacterArray.length;
            let validCharacters = validCharacterArray.join ("");
            if (validCharacters !== currentCharacters)
            {
                createSheet (validCharacters);
                currentCharacters = validCharacters;
            }
        }
    );
    codePointsInput.addEventListener
    (
        'change',
        event =>
        {
            event.currentTarget.value = unicode.charactersToCodePoints (charactersInput.value, true);
        }
    );
   //
    instructions.open = prefs.instructions;
    //
    statistics.open = prefs.statistics;
    //
    const simpleBlockNames =
    {
        "U+4E00..U+9FFF": "CJK Unified (URO)",
        "U+3400..U+4DBF": "CJK Unified Extension A",
        "U+20000..U+2A6DF": "CJK Unified Extension B",
        "U+2A700..U+2B73F": "CJK Unified Extension C",
        "U+2B740..U+2B81F": "CJK Unified Extension D",
        "U+2B820..U+2CEAF": "CJK Unified Extension E",
        "U+2CEB0..U+2EBEF": "CJK Unified Extension F",
        "U+30000..U+3134F": "CJK Unified Extension G",
        "U+31350..U+323AF": "CJK Unified Extension H",
        "U+2EBF0..U+2EE5F": "CJK Unified Extension I",
        "U+F900..U+FAFF": "CJK Compatibility",
        "U+2F800..U+2FA1F": "CJK Compat. Supplement"
    };
    //
    let statisticsHeader = [ "Block Name", ...Object.keys (sources).map ((prefix) => `${prefix}-Source`), "All Sources" ];
    let statisticsData = [ ];
    for (let range in simpleBlockNames)
    {
        let counts = [ ];
        let bounds = range.split ("..");
        let first = parseInt (bounds[0].substring (1), 16);
        let last = parseInt (bounds[1].substring (1), 16);
        for (let prefix in sources)
        {
            let count = 0;
            let property = sources[prefix].property;
            for (let value = first; value <= last; value++)
            {
                let character = String.fromCodePoint (value);
                let codePoint = unicode.characterToCodePoint (character);
                if (codePoints[codePoint] && (property in codePoints[codePoint]))
                {
                    count++;
                }
            }
            counts.push (count);
        }
        statisticsData.push ([ [ simpleBlockNames[range], range ], ...counts, counts.reduce ((a, b) => a + b) ]);
    }
    let glyphsStatisticsTable = require ('./glyphs-statistics-table.js');
    statisticsTable.appendChild (glyphsStatisticsTable.create (statisticsHeader, statisticsData));
    //
    irgSourceRefs.open = prefs.irgSourceRefs;
    //
    let sourceRefsHeader = [ "Code", "Source Reference", "Category" ];
    let sourceRefsData = require ('./irg-source-refs.json');
    let irgSourceRefsTable = require ('./irg-source-refs-table.js');
    irgSourcesTable.appendChild (irgSourceRefsTable.create (sourceRefsHeader, sourceRefsData));
    //
    references.open = prefs.references;
    //
    linksList (links, refLinks);
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        charactersInput: charactersInput.value,
        instructions: instructions.open,
        statistics: statistics.open,
        irgSourceRefs: irgSourceRefs.open,
        references: references.open,
        defaultFolderPath: defaultFolderPath
    };
    context.setPrefs (prefs);
};
//
