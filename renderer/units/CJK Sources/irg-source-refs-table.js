//
module.exports.create = function (sourceRefsHeader, sourceRefsData)
{
    let table = document.createElement ('table');
    table.className = 'sources-table';
    let headerRow = document.createElement ('tr');
    headerRow.className = 'header-row';
    for (let headerString of sourceRefsHeader)
    {
        let header = document.createElement ('th');
        header.textContent = headerString;
        headerRow.appendChild (header);
    }
    table.appendChild (headerRow);
    let sortedCodes = Object.keys (sourceRefsData).sort ();
    for (let code of sortedCodes)
    {
        if (code)
        {
            let dataRow = document.createElement ('tr');
            let codeData = document.createElement ('td');
            codeData.className = 'code-data';
            codeData.textContent = code;
            dataRow.appendChild (codeData);
            let nameData = document.createElement ('td');
            nameData.textContent = sourceRefsData[code].source;
            if (sourceRefsData[code].lang)
            {
                nameData.lang = sourceRefsData[code].lang;
            }
            dataRow.appendChild (nameData);
            let categoryData = document.createElement ('td');
            categoryData.textContent = sourceRefsData[code].category.replace (/\s/gu, "\xA0");
            dataRow.appendChild (categoryData);
            table.appendChild (dataRow);
        }
    }
    return table;
}
//
