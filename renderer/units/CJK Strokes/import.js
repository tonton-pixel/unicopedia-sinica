//
const unitId = 'cjk-strokes-unit';
//
const unit = document.getElementById (unitId);
//
const liveSearch = unit.querySelector ('.live-search');
const wholeWordCheckbox = unit.querySelector ('.whole-word-checkbox');
const sheet = unit.querySelector ('.sheet');
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
const instructions = unit.querySelector ('.instructions');
//
let defaultFolderPath;
//
module.exports.start = function (context)
{
    const linksList = require ('../../lib/links-list.js');
    //
    const regexp = require ('../../lib/unicode/regexp.js');
    const unicode = require ('../../lib/unicode/unicode.js');
    //
    const defaultPrefs =
    {
        liveSearch: "",
        wholeWord: false,
        instructions: true,
        references: false,
        defaultFolderPath: context.defaultFolderPath
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    const strokeCharacters =
    [
        {
            "acronym": "T",
            "stroke": "㇀",
            "unified": ""
        },
        {
            "acronym": "WG",
            "stroke": "㇁",
            "unified": ""
        },
        {
            "acronym": "XG",
            "stroke": "㇂",
            "unified": ""
        },
        {
            "acronym": "BXG",
            "stroke": "㇃",
            "unified": ""
        },
        {
            "acronym": "SW",
            "stroke": "㇄",
            "unified": ""
        },
        {
            "acronym": "HZZ",
            "stroke": "㇅",
            "unified": ""
        },
        {
            "acronym": "HZG",
            "stroke": "㇆",
            "unified": "𠃌"
        },
        {
            "acronym": "HP",
            "stroke": "㇇",
            "unified": ""
        },
        {
            "acronym": "HZWG",
            "stroke": "㇈",
            "unified": ""
        },
        {
            "acronym": "SZWG",
            "stroke": "㇉",
            "unified": ""
        },
        {
            "acronym": "HZT",
            "stroke": "㇊",
            "unified": ""
        },
        {
            "acronym": "HZZP",
            "stroke": "㇋",
            "unified": ""
        },
        {
            "acronym": "HPWG",
            "stroke": "㇌",
            "unified": ""
        },
        {
            "acronym": "HZW",
            "stroke": "㇍",
            "unified": ""
        },
        {
            "acronym": "HZZZ",
            "stroke": "㇎",
            "unified": ""
        },
        {
            "acronym": "N",
            "stroke": "㇏",
            "unified": "乀"
        },
        {
            "acronym": "H",
            "stroke": "㇐",
            "unified": "一"
        },
        {
            "acronym": "S",
            "stroke": "㇑",
            "unified": "丨"
        },
        {
            "acronym": "P",
            "stroke": "㇒",
            "unified": "丿"
        },
        {
            "acronym": "SP",
            "stroke": "㇓",
            "unified": "丿"
        },
        {
            "acronym": "D",
            "stroke": "㇔",
            "unified": "丶"
        },
        {
            "acronym": "HZ",
            "stroke": "㇕",
            "unified": "𠃍"
        },
        {
            "acronym": "HG",
            "stroke": "㇖",
            "unified": "乛"
        },
        {
            "acronym": "SZ",
            "stroke": "㇗",
            "unified": "𠃊"
        },
        {
            "acronym": "SWZ",
            "stroke": "㇘",
            "unified": "𠃎"
        },
        {
            "acronym": "ST",
            "stroke": "㇙",
            "unified": "𠄌"
        },
        {
            "acronym": "SG",
            "stroke": "㇚",
            "unified": "亅"
        },
        {
            "acronym": "PD",
            "stroke": "㇛",
            "unified": "𡿨"
        },
        {
            "acronym": "PZ",
            "stroke": "㇜",
            "unified": "𠃋"
        },
        {
            "acronym": "TN",
            "stroke": "㇝",
            "unified": "乀"
        },
        {
            "acronym": "SZZ",
            "stroke": "㇞",
            "unified": "𠃑"
        },
        {
            "acronym": "SWG",
            "stroke": "㇟",
            "unified": "乚"
        },
        {
            "acronym": "HXWG",
            "stroke": "㇠",
            "unified": "乙"
        },
        {
            "acronym": "HZZZG",
            "stroke": "㇡",
            "unified": "𠄎"
        },
        {
            "acronym": "PG",
            "stroke": "㇢",
            "unified": ""
        },
        {
            "acronym": "Q",
            "stroke": "㇣",
            "unified": ""
        },
        {
            "acronym": "HXG",
            "stroke": "㇤",
            "unified": ""
        },
        {
            "acronym": "SZP",
            "stroke": "㇥",
            "unified": ""
        }
    ];
    //
    function matchString (searchRegex, strokeCharacter)
    {
        let match = false;
        let fields = [ strokeCharacter.acronym, strokeCharacter.stroke, strokeCharacter.unified ];
        for (let field of fields)
        {
            if (searchRegex.test (field))
            {
                match = true;
                break;
            }
        }
        return match;
    }
    //
    function getNameTooltip (character)
    {
        let data = unicode.getCharacterBasicData (character);
        return `<${data.codePoint}>\xA0${data.name}`;
    }
    //
    function createTable (searchString, wholeWord)
    {
        let table = document.createElement ('table');
        table.className = 'table';
        let row = document.createElement ('tr');
        let acronym = document.createElement ('th');
        acronym.className = 'acronym';
        acronym.textContent = "Acronym";
        acronym.title = "Stroke acronym string";
        row.appendChild (acronym);
        let codePoint = document.createElement ('th');
        codePoint.textContent = "Code Point";
        codePoint.className = 'stroke-code-point';
        codePoint.title = "Stroke code point";
        row.appendChild (codePoint);
        let stroke = document.createElement ('th');
        stroke.className = 'stroke';
        stroke.textContent = "Stroke";
        stroke.title = "Code chart representative glyph";
        row.appendChild (stroke);
        let localFont = document.createElement ('th');
        localFont.className = 'local-font';
        localFont.textContent = "Local Font";
        localFont.title = "Default local glyph rendering";
        row.appendChild (localFont);
        let equivalentUnified = document.createElement ('th');
        equivalentUnified.className = 'equivalent-unified';
        equivalentUnified.textContent = "Eq. Unified";
        equivalentUnified.title = "Equivalent CJK unified ideograph";
        row.appendChild (equivalentUnified);
        table.appendChild (row);
        let matchCount = 0;
        let characters = unicode.codePointsToCharacters (searchString);
        if (Array.from (characters).length === 1)
        {
            searchString = characters;
        }
        let searchRegex = regexp.build (searchString, { wholeWord: wholeWord });
        for (let strokeCharacter of strokeCharacters)
        {
            if ((!searchString) || matchString (searchRegex, strokeCharacter))
            {
                matchCount++;
                let row = document.createElement ('tr');
                let acronym = document.createElement ('td');
                acronym.className = 'acronym';
                let acronymString = document.createElement ('span');
                acronymString.className = 'acronym-string';
                acronymString.textContent = strokeCharacter.acronym;
                acronym.appendChild (acronymString);
                row.appendChild (acronym);
                let codePoint = document.createElement ('td');
                codePoint.className = 'stroke-code-point';
                codePoint.textContent = unicode.characterToCodePoint (strokeCharacter.stroke);
                row.appendChild (codePoint);
                let stroke = document.createElement ('td');
                stroke.className = 'stroke';
                let strokeGlyph = document.createElement ('span');
                strokeGlyph.className = 'glyph';
                strokeGlyph.textContent = strokeCharacter.stroke;
                stroke.appendChild (strokeGlyph);
                let strokeCodePoint = document.createElement ('span');
                strokeCodePoint.className = 'code-point';
                strokeCodePoint.textContent = unicode.characterToCodePoint (strokeCharacter.stroke);
                stroke.appendChild (strokeCodePoint);
                stroke.title = getNameTooltip (strokeCharacter.stroke);
                row.appendChild (stroke);
                let localFont = document.createElement ('td');
                localFont.className = 'local-font';
                let localGlyph = document.createElement ('span');
                localGlyph.className = 'glyph';
                localGlyph.textContent = strokeCharacter.stroke;
                localFont.appendChild (localGlyph);
                let localCodePoint = document.createElement ('span');
                localCodePoint.className = 'code-point';
                localCodePoint.textContent = unicode.characterToCodePoint (strokeCharacter.stroke);
                localFont.appendChild (localCodePoint);
                localFont.title = getNameTooltip (strokeCharacter.stroke);
                row.appendChild (localFont);
                let equivalentUnified = document.createElement ('td');
                equivalentUnified.className = 'equivalent-unified';
                if (strokeCharacter.unified)
                {
                    let unifiedGlyph = document.createElement ('span');
                    unifiedGlyph.className = 'glyph';
                    unifiedGlyph.textContent = strokeCharacter.unified;
                    equivalentUnified.appendChild (unifiedGlyph);
                    let unifiedCodePoint = document.createElement ('span');
                    unifiedCodePoint.className = 'code-point';
                    unifiedCodePoint.textContent = unicode.characterToCodePoint (strokeCharacter.unified);
                    equivalentUnified.appendChild (unifiedCodePoint);
                    equivalentUnified.title = getNameTooltip (strokeCharacter.unified);
                }
                else
                {
                    equivalentUnified.classList.add ('empty');
                }
                row.appendChild (equivalentUnified);
                table.appendChild (row);
            }
        }
        if (matchCount === 0)
        {
            let row = document.createElement ('tr');
            let message = document.createElement ('td');
            message.className = 'message';
            message.setAttribute ('colspan', 5); // !!
            message.textContent = "No Match";
            row.appendChild (message);
            table.appendChild (row);
        }
        table.appendChild (row.cloneNode (true));
        return table;
    }
    //
    function doSearch (string, wholeWord)
    {
        while (sheet.firstChild)
        {
            sheet.firstChild.remove ();
        }
        sheet.appendChild (createTable (string, wholeWord));
    }
    //
    wholeWordCheckbox.checked = prefs.wholeWord;
    wholeWordCheckbox.addEventListener
    (
        'input',
        (event) => doSearch (liveSearch.value, event.currentTarget.checked)
    );
    //
    liveSearch.value = prefs.liveSearch;
    liveSearch.addEventListener ('input', (event) => { doSearch (event.currentTarget.value, wholeWordCheckbox.checked); });
    //
    doSearch (liveSearch.value, wholeWordCheckbox.checked);
    //
    const refLinks = require ('./ref-links.json');
    //
    instructions.open = prefs.instructions;
    //
    references.open = prefs.references;
    //
    linksList (links, refLinks);
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        liveSearch: liveSearch.value,
        wholeWord: wholeWordCheckbox.checked,
        instructions: instructions.open,
        references: references.open,
        defaultFolderPath: defaultFolderPath
    };
    context.setPrefs (prefs);
};
//
