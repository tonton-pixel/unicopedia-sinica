// Unzip CJK Variations Data Files to User Data Directory
const path = require ('path');
const extract = require ('extract-zip');
// Note: the relevant zip file (downloadable from <https://codeberg.org/tonton-pixel/svg-glyphs-2022>) is assumed to be in the Downloads folder.
const source = path.join ($.getpath ('downloads'), "svg-glyphs-2022-master.zip");
const target = $.getpath ('userData');
async function main ()
{
    try
    {
        await extract (source, { dir: target });
        $.clear ();
        $.writeln ("Extraction completed.");
    }
    catch (err)
    {
        $.clear ();
        $.writeln ("Extraction failed:", err);
    }
}
$.writeln ("Please wait...");
main ();
