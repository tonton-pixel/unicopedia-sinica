// List CJK Strokes in IDS
const { characters } = require ('./lib/unicode/cjk-ids.json');
const { characterToCodePoint } = require ('./lib/unicode/unicode.js');
let strokeList = [ ];
for (let character in characters)
{
    let idsElements = [ ];
    let sources = characters[character];
    for (let source in sources)
    {
        if (source !== "*")
        {
            let ids = sources[source];
            if (Array.isArray (ids))
            {
                idsElements.push (...ids);
            }
            else
            {
                idsElements.push (ids);
            }
        }
    }
    for (let idsElement of idsElements)
    {
        let strokes = idsElement.match (/([㇀-㇥])/gu);
        if (strokes)
        {
            strokeList.push (...strokes);
        }
    }
}
let uniqueStrokes = [...new Set (strokeList)].sort ();
$.writeln ("Count:", uniqueStrokes.length);
for (let stroke of uniqueStrokes)
{
    $.writeln (characterToCodePoint (stroke), stroke);
}
