// List IDS Indivisible Characters
const { characters } = require ('./lib/unicode/cjk-ids.json');
const { characterToCodePoint } = require ('./lib/unicode/unicode.js');
const { coreSet, core2020Set } = require ('./lib/unicode/parsed-unihan-data.js');
function getCharacterSet (codePoint)
{
    let set = "Full";
    if (coreSet.includes (codePoint))
    {
        set = "IICore";
    }
    else if (core2020Set.includes (codePoint))
    {
        set = "U-Core";
    }
    return set;
}
let indivisibleCharacters = [ ];
for (let character in characters)
{
    let idsElements = [ ];
    let sources = characters[character];
    let codePoint = characterToCodePoint (character);
    for (let source in sources)
    {
        if (source !== "*")
        {
            let ids = sources[source];
            if (Array.isArray (ids))
            {
                idsElements.push (...ids);
            }
            else
            {
                idsElements.push (ids);
            }
        }
    }
    for (let idsElement of idsElements)
    {
        if (idsElement === character)
        {
            indivisibleCharacters.push (`${codePoint}\t${character}\t${getCharacterSet (codePoint)}`);
            break;
        }
    }
}
$.writeln ("Count:", indivisibleCharacters.length);
$.writeln (indivisibleCharacters.join ("\n"));
