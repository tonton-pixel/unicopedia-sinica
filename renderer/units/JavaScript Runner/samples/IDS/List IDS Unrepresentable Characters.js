// List IDS Unrepresentable Characters
const { characters } = require ('./lib/unicode/cjk-ids.json');
const { characterToCodePoint } = require ('./lib/unicode/unicode.js');
const { coreSet, core2020Set } = require ('./lib/unicode/parsed-unihan-data.js');
function getCharacterSet (codePoint)
{
    let set = "Full";
    if (coreSet.includes (codePoint))
    {
        set = "IICore";
    }
    else if (core2020Set.includes (codePoint))
    {
        set = "U-Core";
    }
    return set;
}
let unrepresentableCharacters = [ ];
for (let character in characters)
{
    let idsElements = [ ];
    let sources = characters[character];
    let codePoint = characterToCodePoint (character);
    for (let source in sources)
    {
        if (source !== "*")
        {
            let ids = sources[source];
            if (Array.isArray (ids))
            {
                idsElements.push (...ids);
            }
            else
            {
                idsElements.push (ids);
            }
        }
    }
    for (let idsElement of idsElements)
    {
        if (/？/u.test (idsElement))
        {
            unrepresentableCharacters.push ({ character, ids: idsElement, set: getCharacterSet (codePoint) } );
        }
    }
}
$.writeln ("Please wait...");
setTimeout
(
    () =>
    {
        $.clear ();
        $.writeln ("Count:", unrepresentableCharacters.length);
        for (let character of unrepresentableCharacters)
        {
            $.writeln (`${characterToCodePoint (character.character)}\t${character.character}\t${character.set}\t${character.ids}`);
        }
    }
);
