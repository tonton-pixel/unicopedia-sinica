// List Radicals in IDS
const { characters } = require ('./lib/unicode/cjk-ids.json');
const { characterToCodePoint } = require ('./lib/unicode/unicode.js');
let radicalList = [ ];
for (let character in characters)
{
    let idsElements = [ ];
    let sources = characters[character];
    for (let source in sources)
    {
        if (source !== "*")
        {
            let ids = sources[source];
            if (Array.isArray (ids))
            {
                idsElements.push (...ids);
            }
            else
            {
                idsElements.push (ids);
            }
        }
    }
    for (let idsElement of idsElements)
    {
        let radicals = idsElement.match (/(\p{Radical})/gu);
        if (radicals)
        {
            radicalList.push (...radicals);
        }
    }
}
let uniqueRadicals = [...new Set (radicalList)].sort ();
$.writeln ("Count:", uniqueRadicals.length);
for (let radical of uniqueRadicals)
{
    $.writeln (characterToCodePoint (radical), radical);
}
